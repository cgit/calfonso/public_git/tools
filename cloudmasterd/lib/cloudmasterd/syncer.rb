# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/usr/bin/ruby

require 'rubygems'
require 'open3'
require 'sqlite3'
require 'active_record'
require 'date'
require 'yaml'

# This is needed to let a 'kill PID' command cleanly
# kill the process without requiring -9
Signal.trap("TERM") do
  # Clean up any active record connections
  ActiveRecord::Base.remove_connection

  # Cleanly exit
  exit 0
end

# Define the ActiveRecord classes
class Machine < ActiveRecord::Base
  set_table_name "cloudmasterd_machine"
end

class Cloud < ActiveRecord::Base
  set_table_name "cloudmasterd_cloud"
end

class Syncer
  # This method makes a simple attempt to provide some locking
  def self.lock
    lockfile = '/var/tmp/cloudmasterd.lock'

    begin
      # Wait until the lock is released
      while File.exists?(lockfile) do
        sleep 0.5
      end

      # Obtain the lock
      File.open(lockfile, "w") {|file| file.puts Thread.object_id.to_s}

      # Make sure the lock is ours - otherwise retry
      retry unless File.open(lockfile, "r").readline.chomp. == Thread.object_id.to_s

      # Do the lock processing
      yield
    ensure
      # Make sure to delete the lock
      File.delete(lockfile)
    end
  end

  def sync_memory
    current_state = {}
    Open3.popen3("func '*' call virt freemem") do |stdin, stdout, stderr|
      YAML.load(stdout.read).each do |host, value|
        current_state[host] = { :memory => value }
      end
    end

    Open3.popen3("func '*' call virt virttype") do |stdin, stdout, stderr|
      YAML.load(stdout.read).each do |host, type|
        current_state[host][:virttype] = type
      end
    end

    Cloud.transaction do
      # Sync up the DB state - delete everything first
      Cloud.delete_all

      # Create the current entries
      current_state.each do |host, attrs|
        Cloud.create :name => host, :memory => attrs[:memory], :virttype => attrs[:virttype], :added_date => DateTime.now()
      end
    end
  end

  def sync_state
    current_state = {}
    Open3.popen3("func '*' call virt state") do |stdin, stdout, stderr|
      YAML.load(stdout.read).each do |host, machines|
        machines.each do |machine|
          name, state = machine.split(" ")
          current_state[name] = {:state => state, :cloud => host} unless name == "Domain-0"
        end
      end
    end

    # Sync up the state for all the machines
    Machine.find(:all).each do |machine|
      if current_state.has_key?(machine.name) then
        # We have a current state match for this machine
        machine.update_attributes(:cloud => current_state[machine.name][:cloud], :state => current_state[machine.name][:state])

        # Delete the key so we can determine unaccounted for machines
        current_state.delete(machine.name)
      else
        # Not good - the machine in the DB isn't locateable
        machine.update_attribute(:state, "missing")
      end
    end

    # We need to create an 'unknown' instance for all the remaining keys
    current_state.each do |name, machine|
      Machine.create :name => name, :email => "unknown", :cloud => machine[:cloud], :state => machine[:state]
    end
  end

  def sync_owners
    # Sync up any unowned machines that now have an owner (handles race condition with syncing states)
    unowned_machines = {}
    Machine.find(:all, :conditions => "email = 'unknown'").each do |machine|
      unowned_machines[machine.name] = machine
    end

    owned_machines = {}
    Machine.find(:all, :conditions => "email <> 'unknown'").each do |machine|
      owned_machines[machine.name] = machine
    end

    owned_machines.each do |name, machine|
      if unowned_machines.has_key?(name) then
        # Delete the unowned machine and update the owned one
        u_machine = unowned_machines[name]
        machine.update_attributes(:cloud => u_machine.cloud, :state => u_machine.state)
        u_machine.destroy()
      end
    end
  end

  def run
    # Wait until the DB is there to establish the connection
    until File.exists?("/var/lib/cloudmaster.db") do
      sleep 10
    end

    ActiveRecord::Base.logger = Logger.new('/var/log/cloudmasterd-sync.log', 5, 1024000)
    ActiveRecord::Base.colorize_logging = false

    # Setup the database connection
    ActiveRecord::Base.establish_connection(
        :adapter => "sqlite3",
        :dbfile  => "/var/lib/cloudmaster.db"
    )

    # Constantly keep the database in sync
    while true do
      begin
        Syncer::lock { sync_memory }
        Syncer::lock { sync_state }
        Syncer::lock { sync_owners }
      rescue Exception => e
        puts "ERROR: An error occured during syncing - #{e.to_s}"
      end

      sleep 5
    end
  end
end
