cloudmasterd
============

The cloudmasterd daemon runs by default on port 8107 that is proxy passed by
apache from the root context.  This daemon provides a REST style interface to
both create new machine instances and manage the existing machine instances.
It also provides a view of the cloud instances available and the amount of
free memory that is available.  A cloud consists of multiple cloud instances
(e.g. cloud1.example.org, cloud2.example.org) which in turn host multiple
machine instances (e.g. jboss1.example.org, jboss2.example.org).

Func (https://fedorahosted.org/func) is used to aggregate all the information
from the individual cloud instances to store in a sqlite database on the
cloud master.

Installing
===========

The cloudmasterd service is dependent on several subsystems so in general,
the easiest way to setup a cloud master and a series of cloud instances is
to utilize the genome-replace-self scripts and rekickstart the machine.
You can do this by downloading the genome-replace-self from an internal or
public cobbler machine using:

  rpm -i http://$COBBLER_REPO/cobbler/repo_mirror/f8-genome-noarch/genome-replace-self-1.0.0-1.noarch.rpm

and then running the following replace self script which will reformat your
machine as either a cloud master or a cloud minion.

To create a cloud master, you would use the following command:

  genome-replace-self -c $COBBLER_REPO

To create a cloud minion, you would use the following command:

  genome-replace-self -c $COBBLER_REPO -m "certmaster=$CLOUD_MASTER"

Both forms of the command also support passing in specific cobbler profiles
to allow you to create a RHEL based cloud or use different architectures.
The default profile used is a Fedora 8, 32 bit profile.  For example, to use
RHEL5 Update 2 with a 64 bit architecture, you could create a cloud master
using:

  genome-replace-self -c $COBBLER_REPO -p GenomeCloud-RHEL5U2-x86_64

The cloudmasterd is also available as a rubygem RPM and can be installed using:
  yum install rubygem-cloudmasterd
