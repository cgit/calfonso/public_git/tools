# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#!/usr/bin/env ruby

require 'rubygems'
require 'sqlite3'
require 'active_record'

# This is an example of how you can prime the cloudmaster database with
# values to allow for testing without actually having func running
ActiveRecord::Base.logger = Logger.new(STDOUT)
ActiveRecord::Base.colorize_logging = true

ActiveRecord::Base.establish_connection(
    :adapter => "sqlite3",
    :dbfile  => "/var/lib/cloudmaster.db"
)

class Machine < ActiveRecord::Base
  set_table_name "cloudmasterd_machine"
end

class Cloud < ActiveRecord::Base
  set_table_name "cloudmasterd_cloud"
end

# Create the cloud instances
Cloud.create :name => "cloud1.example.org", :memory => 1024
Cloud.create :name => "cloud2.example.org", :memory => 512
Cloud.create :name => "cloud3.example.org", :memory => 16125

# Create the corresponding machines
Machine.create :name => "system1.example.org", :email => "user1@example.org", :cloud => "cloud1.example.org", :state => "Installing"
Machine.create :name => "system2.example.org", :email => "user1@example.org", :cloud => "cloud1.example.org", :state => "Installing"
Machine.create :name => "system3.example.org", :email => "user1@example.org", :cloud => "cloud2.example.org", :state => "Installing"
Machine.create :name => "system4.example.org", :email => "user2@example.org", :cloud => "cloud3.example.org", :state => "Installing"
Machine.create :name => "system5.example.org", :email => "user2@example.org", :cloud => "cloud2.example.org", :state => "Installing"
