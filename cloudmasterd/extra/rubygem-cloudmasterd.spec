%define ruby_sitelib %(ruby -rrbconfig -e "puts Config::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname cloudmasterd
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Genome library and web application for managing cloud machines
Name: rubygem-%{gemname}
Version: 1.0.0
Release: 15%{?dist}
Group: Applications/System
License: GPLv2
URL: https://fedorahosted.org/genome
Source0: https://fedorahosted.org/releases/g/e/genome/%{gemname}-%{version}.gem
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: sqlite
Requires: genome-func
Requires: genome-styling
Requires: genome-autostarter
Requires: koan
Requires: python-virtinst
Requires: /usr/bin/virsh
Requires: rubygems
Requires: rubygem(picnic) 
Requires: rubygem(activesupport) 
Requires: rubygem(activerecord) 
Requires: rubygem(sqlite3-ruby) 
Requires: rubygem(reststop) >= 0.2.1
Requires: genome-styling
Requires(post): chkconfig
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
The cloudmasterd daemon runs by default on port 8107 that is proxy passed by
apache from the root context.  This daemon provides a REST style interface to 
both create new machine instances and manage the existing machine instances.
It also provides a view of the cloud instances available and the amount of
free memory that is available.  A cloud consists of multiple cloud instances
(e.g. cloud1.example.org, cloud2.example.org) which in turn host multiple
machine instances (e.g. jboss1.example.org, jboss2.example.org).

Func (https://fedorahosted.org/func) is used to aggregate all the information
from the individual cloud instances to store in a sqlite database on the
cloud master. 

%package -n httpd-cloudmasterd
Summary: ProxyPass support for the cloudmaster
Requires: httpd
Group: Applications/System

%description -n httpd-cloudmasterd
In order to abstract the port that the cloudmaster runs on,
this RPM enables ProxyPass support from httpd to the cloudmaster
daemon running on port 8107.

%package -n httpd-cloudmasterd-redirect
Summary: Enables redirecting the root context for the cloudmaster
Requires: httpd
Group: Applications/System

%description -n httpd-cloudmasterd-redirect
When this RPM is installed, the default context is redirected
to the cloudmaster context.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir} %{buildroot}%{_sysconfdir}/init.d %{buildroot}%{_sysconfdir}/cloudmasterd
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
mv %{buildroot}%{geminstdir}/extra/cloudmasterd.init %{buildroot}%{_sysconfdir}/init.d/cloudmasterd
mv %{buildroot}%{geminstdir}/extra/config.yml %{buildroot}%{_sysconfdir}/cloudmasterd/config.yml
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

# Put in the apache rules
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
mv %{buildroot}%{geminstdir}/extra/cloudmasterd.apache %{buildroot}%{_sysconfdir}/httpd/conf.d/cloudmasterd.conf
mv %{buildroot}%{geminstdir}/extra/cloudmasterd-redirect.apache %{buildroot}%{_sysconfdir}/httpd/conf.d/cloudmasterd-redirect.conf

%post
# Register the httpd service
/sbin/chkconfig --add cloudmasterd

%preun
if [ $1 = 0 ]; then
        /sbin/service cloudmasterd stop > /dev/null 2>&1
        /sbin/chkconfig --del cloudmasterd
fi

%clean
rm -rf %{buildroot}

%files
%config(noreplace) %{_sysconfdir}/cloudmasterd/config.yml
%{_bindir}/cloudmasterd
%{_bindir}/cloudmasterd-ctl
%{_bindir}/cloudmasterd-sync
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%defattr(755, root, root, -)
%{_sysconfdir}/init.d/cloudmasterd

%files -n httpd-cloudmasterd
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README
%config(noreplace) %{_sysconfdir}/httpd/conf.d/cloudmasterd.conf

%files -n httpd-cloudmasterd-redirect
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README
%config(noreplace) %{_sysconfdir}/httpd/conf.d/cloudmasterd-redirect.conf

%changelog
* Mon Jul 21 2008 Jeroen van Meeuwen <kanarip@fedoraproject.org> - 1.0.0-15
- Some cleaning up for package review

* Tue Jun 24 2008  <mhicks@redhat.com> - 1.0.0-1
- Initial package
