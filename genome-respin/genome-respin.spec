Summary: Genome Respins
Name: genome-respin
Source: genome-respin-bin.tar.gz
Version: 1.0.1
Release: 8%{?dist}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Group: Applications/System
URL: https://fedorahosting.org/genome
License: GPLv2
Requires: revisor

%description
The configurations that support building Genome respins
using Revisor.

%prep

%setup -c

%build

%install
# Cleaning up the build root
rm -rf $RPM_BUILD_ROOT

# Create the directory structure required to lay down our files
mkdir -p $RPM_BUILD_ROOT/etc/revisor
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1

cp -R genome.conf $RPM_BUILD_ROOT/etc/revisor
cp -R conf.d $RPM_BUILD_ROOT/etc/revisor
cp -R docs/* $RPM_BUILD_ROOT/%{_mandir}/man1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_mandir}/man1/genome-respin.1.gz
/etc/revisor/genome.conf
/etc/revisor/conf.d/genome-ks.cfg
/etc/revisor/conf.d/cloud-ks.cfg
/etc/revisor/conf.d/combo-ks.cfg
/etc/revisor/conf.d/genome-f9-i386.conf

%doc
