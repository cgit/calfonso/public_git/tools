Name:           genome-bridge
Version:        1.0.1
Release:        2%{?dist}
Summary:        Genome network bridge
Group:          System Environment/Base
License:        GPLv2
URL:            http://genome.et.redhat.com/
Source0:        genome-bridge
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Requires: 	bridge-utils

%description
Genome network bridging for kvm

%prep
%setup -c -T

%build

%install
rm -rf $RPM_BUILD_ROOT

install -Dpm 755 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/init.d/genome-bridge

%post
/sbin/chkconfig --add %{name}

%preun
/sbin/chkconfig --del %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_sysconfdir}/init.d/genome-bridge

%changelog
* Wed Jul 16 2008 Brenton Leanhardt <bleanhar@redhat.com> - 1.0.0-2
- initial pkg
