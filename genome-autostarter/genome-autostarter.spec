Summary: Genome virtual machine autostarter
Name: genome-autostarter
Source0: genome-autostarter
Source1: genome-autostarter.sh
Version: 1.0.0
Release: 7%{?dist}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Group: Applications/System
URL: http://genome.et.redhat.com
License: GPLv2

%description
Genome virtual machine autostarter

%prep

%setup -c -T

%build

%install
# Cleaning up the build root
rm -rf $RPM_BUILD_ROOT

# Create the directory structure required to lay down our files
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/cron.d $RPM_BUILD_ROOT/%{_sbindir}

install -pm 644 %{SOURCE0} $RPM_BUILD_ROOT/%{_sysconfdir}/cron.d
install -pm 755 %{SOURCE1} $RPM_BUILD_ROOT/%{_sbindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_sysconfdir}/cron.d/genome-autostarter
%{_sbindir}/genome-autostarter.sh
%doc

%changelog
* Tue Jul 22 2008 Jeroen van Meeuwen <kanarip@fedoraproject.org> - 1.0.0-7
- Initial build
