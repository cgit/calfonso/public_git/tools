#!/bin/bash
uname -a | grep -i xen
if [ $? == 0 ]; then
       	# We're using Xen
	find /etc/xen/auto -name "*" -type l | xargs -n1 basename 2> /dev/null | xargs -n1 virsh start &> /dev/null
else
	# We're using kvm
	for vm_xml in `find /etc/libvirt/qemu/autostart -name "*" -type l 2> /dev/null`; do
           vm=`basename $vm_xml .xml` # Chop of the extension
           virsh start $vm &> /dev/null
	done
fi
