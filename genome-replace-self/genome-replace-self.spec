Summary: Genome Replace Self Utility
Name: genome-replace-self
Source: genome-replace-self-bin.tar.gz
Version: 1.0.0
Release: 2%{?dist}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Group: Applications/System
URL: https://fedorahosted.org/genome
License: GPLv2

%description
Genome scripts to bring a system to a certain state
to allow a koan --replace-self to be run on the machine
using a default or specified profile.

%prep

%setup -c

%build

%install
# Cleaning up the build root
rm -rf $RPM_BUILD_ROOT

# Create the directory structure required to lay down our files
mkdir -p $RPM_BUILD_ROOT/usr/bin

cp -R . $RPM_BUILD_ROOT/usr/bin

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/bin/genome-replace-self
/usr/bin/genome-replace-self.rb

%doc
