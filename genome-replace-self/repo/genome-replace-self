#!/bin/bash

LOG=/var/log/genome-replace-self.log
RHEL4_REPO=http://download.fedora.redhat.com/pub/epel/4/i386/epel-release-4-9.noarch.rpm
RHEL5_REPO=http://download.fedora.redhat.com/pub/epel/5/i386/epel-release-5-3.noarch.rpm
FEDORA_REPO=http://download.fedora.redhat.com/pub/fedora/linux/releases/8/Everything/i386/os/Packages/fedora-release-8-3.noarch.rpm
DEFAULT_PROFILE=GenomeCloud-F8-i386

if [ "$USER" != "root" ]; then
  echo "You need to run this as root"
  exit 1
fi

#
# Check for options and print out help if necessary
#
USAGE="""
Usage: genome-replace-self -[c]obbler_server -[p]rofile -[m]etadata

where options include:
    -c (required)  the cobbler server from which to provision this machine
    -p (optional)  a specific profile to use for this machine
    -m (optional)  the metadata to pass to the cobbler system"""

while getopts ":n:c:m:p:" opt; do
  case $opt in
    c  ) COBBLER="$OPTARG" ;;
    p  ) PROFILE="$OPTARG" ;;
    m  ) METADATA="$OPTARG" ;;
    * ) echo -e "$USAGE"
        exit 1
  esac
done

shift $(($OPTIND - 1))

if [ -z "$COBBLER" ]; then
  echo "ERROR: The cobbler option is required"
  echo -e "$USAGE"
  exit 1
fi

if [ -z "$PROFILE" ]; then
  PROFILE="$DEFAULT_PROFILE"
fi

if [ -z "$METADATA" ]; then
  METADATA="certmaster=localhost"
fi

echo "WARNING: This is going to completely reformat your system."
echo "Proceed? [y/N]:"
read confirm

if [ "$confirm" != "y" ]; then
  exit 1
fi

echo "Checking for required dependencies..."

which ruby &> $LOG
RESULT=$?

if [ "$RESULT" == "1" ]; then
  echo "Upgrading ruby..."
  yum install -y ruby &> /dev/null
fi

which ruby &> $LOG
RESULT=$?

if [ "$RESULT" == "1" ]; then
  echo "ERROR: Please install ruby manually and try again."
  exit 1
fi

# Check the version of koan
rpm -q koan | grep "koan-1\.*" &> $LOG
RESULT=$?

if [ "$RESULT" == "1" ]; then
  echo "Upgrading koan..."
  echo "Cleaning up current yum repositories to get the right version of koan..."

  rpm -e --force epel-release &> $LOG
  rm -rf /etc/yum.repos.d/* &> $LOG
  yum clean all &> $LOG

  echo "Installing the yum repository required for koan..."

  # Install EPEL for the right version of RHEL
  if [ "`grep 'Red Hat.*4' /etc/redhat-release`" != "" ]; then
    rpm -Uvh --force $RHEL4_REPO &> $LOG
  elif [ "`grep 'Red Hat.*5' /etc/redhat-release`" != "" ]; then
    rpm -Uvh --force $RHEL5_REPO &> $LOG
  elif [ "`grep 'Fedora' /etc/redhat-release`" != "" ]; then
    rpm -Uvh --force $FEDORA_REPO &> $LOG
  elif [ "`grep 'Red Hat' /etc/redhat-release`" != "" ]; then
    echo "ERROR: Unknown operating system - please install koan manually."
    exit 1
  else
    echo "Not RHEL 4 or 5, assuming koan is available through yum"
  fi

  yum install -y koan &> $LOG
fi

# Double-check koan version
rpm -q koan | grep "koan-1\.*" &> $LOG
RESULT=$?

if [ "$RESULT" == "1" ]; then
  echo "ERROR: Please install koan >= 1.0 manually and try again."
  exit 1
fi

# Now, call out to our script to do the cobbler system registration and koan
ruby /usr/bin/genome-replace-self.rb $COBBLER $PROFILE $METADATA &> $LOG
RESULT=$?
if [ "$RESULT" == "1" ]; then
  echo "ERROR: An unknown error occured, please check $LOG for details"
else
  echo "You can now reboot your system and that will begin the reformatting."
fi
