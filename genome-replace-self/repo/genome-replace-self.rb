# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'xmlrpc/client'

# Parse out the arguments
cobbler = ARGV[0]
profile = ARGV[1]
metadata = ARGV[2]
mac_addr = `/sbin/ifconfig eth0 | grep HWaddr | awk '{print $5}'`.chomp

cobblerd = XMLRPC::Client.new2("http://#{cobbler}/cobbler_api_rw")

# TODO: figure out a way to pass these values in
token = cobblerd.call2("login", "cobbler", "password")[1]

# Register the system with the MAC address as the name
system_id = cobblerd.call2("new_system", token)[1]
cobblerd.call2('modify_system', system_id, 'name', mac_addr, token)
cobblerd.call2('modify_system', system_id, 'profile', profile, token)

cobblerd.call2('modify_system', system_id, 'ksmeta', metadata, token)
cobblerd.call2('save_system', system_id, token)
    
koan_command = "/usr/bin/koan -s #{cobbler} --system=#{mac_addr} --replace-self"
`#{koan_command}`
