# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'lib/genome-dsl/machines'
require 'test/genome-test'
require 'test/unit'

# We have to do this before including the module
GenomeDsl::GenomeDsl[:machine_types_file] = File.dirname(__FILE__) + "/data/sparse-dsl.rb"


class TestSparseDsl < Test::Unit::TestCase
  include GenomeDsl
  include GenomeTest

  def test_dsl_load
    assert_equal(machines.size, 1)
  end

  def test_machines
    m = assert_machine("web-build", 1, 1, false)
    assert(m.classes.find {|c| c == "genome::tomcat::build"})
    assert(m.facts.find {|f| f.name == "gitserver"})
  end
end
