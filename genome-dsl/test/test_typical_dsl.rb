# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'lib/genome-dsl/machines'
require 'test/genome-test'
require 'test/unit'

# We have to do this before including the module
GenomeDsl::GenomeDsl[:machine_types_file] = File.dirname(__FILE__) + "/data/typical-dsl.rb"

class TestTypicalDsl < Test::Unit::TestCase
  include GenomeDsl
  include GenomeTest 

  def test_dsl_load
    assert_equal(8, machines.size)
  end

  def test_machines
    m1 = assert_machine("typical-machine", 2, 3)
    assert(m1.classes.find {|c| c == "genome::tomcat::apache_proxy"})

    assert_machine("no-explicit-facts-or-classes", 2, 1)
    assert_machine("no-block", 2, 1)
    assert_machine("include-globals-explicitly", 2, 1)
  end

  def test_external_node_yaml
    assert_equal(<<YAML, external_node_yaml("missing-machine.usersys.redhat.com"))
--- []

YAML

    assert_equal(<<YAML, external_node_yaml("bleanhar-typical-machine.usersys.redhat.com"))
--- 
parameters: {}

classes: 
- puppet::client
- genome::tomcat::apache_proxy
- genome::tomcat::apache_static
YAML

    assert_equal(<<YAML, external_node_yaml("bleanhar-testing-order.usersys.redhat.com"))
--- 
parameters: {}

classes: 
- puppet::client
- puppet::testing::order
YAML

    assert_equal(<<YAML, external_node_yaml("bleanhar-really-testing-order.usersys.redhat.com"))
--- 
parameters: {}

classes: 
- puppet::client
- puppet::really::testing::order
YAML
  end
end
