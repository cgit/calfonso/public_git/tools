# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

module GenomeTest
  def assert_machine(name, num_facts, num_classes, globals=true)
    m = machines.find {|m| m.name == name}
    assert_globals(m) if globals

    assert_equal(num_facts, m.facts.size)
    assert(! m.facts.any? {|f| f.nil?})

    assert_equal(num_classes, m.classes.size)
    assert(! m.classes.any? {|c| c.nil?})
    return m
  end

  def assert_globals(machine)
    assert(machine.facts.find {|f| f.name == "puppetserver"})
    assert(machine.facts.find {|f| f.name == "logserver"})
    assert_equal("puppet::client", machine.classes[0])
  end
end
