# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# This DSL doesn't use global facts or classes
#
# Test facts
newfact("gitserver") do
  set_desc "The machine hosting the bare git repositories for this machine"
  set_default "%repo%-repo.usersys.redhat.com"
end

# Test machine types
newmachine("web-build") do
  include_facts "gitserver"
  set_classes "genome::tomcat::build"
  set_desc "This machine type comes installed with all the development tools " +
                   "and code needed for building the tomcat based web stack"
end
