# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Test facts
newfact("puppetserver", :on_all_nodes => true) do
  set_desc "The puppetmaster used for all configuration.  If you are " +
           "provisioning a Repo machine, this should be 'localhost'"
  set_default "%repo%-repo.usersys.redhat.com"
end

newfact("logserver", :on_all_nodes => true) do
  set_desc "server used for syslogd.  Another safe value is 'localhost'"
  set_default "%machine_name%-host.usersys.redhat.com"
end

newfact("gitserver") do
  set_desc "The machine hosting the bare git repositories for this machine"
  set_default "%repo%-repo.usersys.redhat.com"
end

# Test machine types
classes_on_all_machine_types "puppet::client"

newmachine("web-build") do
  include_facts "gitserver"
  set_classes "genome::tomcat::build"
  set_desc "This machine type comes installed with all the development tools " +
                   "and code needed for building the tomcat based web stack"
end

newmachine("typical-machine") do
  include_facts "logserver", "puppetserver"
  set_classes "puppet::client", "genome::tomcat::apache_proxy", "genome::tomcat::apache_static"
  set_desc "Proxy for web-apps and web-services machines"
end

newmachine("include-globals-explicitly") do
  include_facts "logserver", "puppetserver"
  set_classes "puppet::client"
  set_desc "Proxy for web-apps and web-services machines"
end

newmachine("no-explicit-facts-or-classes") {}
newmachine("no-block")

newmachine("really-testing-order") do
  set_desc "Previously we had problems with regex ordering"
  set_classes "puppet::really::testing::order"
end

newmachine("testing-order") do
  set_desc "Previously we had problems with regex ordering"
  set_classes "puppet::testing::order"
end

newmachine("really-testing-order-again") do
  set_desc "Previously we had problems with regex ordering"
  set_classes "puppet::really::testing::order::again"
end
