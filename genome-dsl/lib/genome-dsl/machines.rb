# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'yaml'

module GenomeDsl
  GenomeDsl = {:machine_types_file => "/etc/genome/machine_types.rb"}

  module DSL
    def newmachine(name, &block)
      @machines ||= Array.new
      @machines << Machine.new(name, self, &block) 
    end

    def newfact(name, options={}, &block)
      @facts ||= Array.new
      @global_facts ||= Array.new

      if options[:on_all_nodes]
        @global_facts << Fact.new(name, &block)
      else
        @facts << Fact.new(name, &block) 
      end
    end

    def classes_on_all_machine_types(*classes)
      @global_classes ||= Array.new
      @global_classes.concat(classes).uniq!
    end

    # If we have to duplicate this one more time I vote for meta-programming it
    # away.
    def machines
      @machines ||= Array.new
      @machines.dup
    end

    def global_classes
      @global_classes ||= Array.new
      @global_classes.dup
    end

    def global_facts
      @global_facts ||= Array.new
      @global_facts.dup
    end

    def lookup_fact(fact)
      @facts ||= Array.new
      @facts.find {|f| f.name == fact}
    end
  end

  # just delegate to the DSL
  def machines
    self.class.machines
  end

  def external_node_yaml(hostname)
    # acc, or Accumulator is the best match so far
    machine = machines.inject(machines.first) do |acc, m| 
      # Find the regex that matches the most characters
      if m.matching_chars(hostname) > acc.matching_chars(hostname)
        acc = m
      end
      acc # return
    end

    #At this point we either have the best match,
    #or we have no match at all
    if machine.matches?(hostname)
      h = Hash.new
      h["classes"] = machine.classes
      h["parameters"] = machine.parameters
      return YAML::dump(h)
    else
      return YAML::dump([])
    end
  end

  def self.included(mod)
    # Create the needed class methods for the eval
    mod.extend(DSL)

    # If the file exists, run the DSL whenever this module is included
    mod.instance_eval(File.read(GenomeDsl[:machine_types_file])) if File.exist?(GenomeDsl[:machine_types_file])
  end

  class Fact
    attr_reader :name, :desc, :default

    def initialize(name, &block)
      @name = name
      @desc = String.new
      @default = String.new
      instance_eval(&block)
    end

    def set_desc(d)
      @desc = d
    end

    def set_default(d)
      @default = d
    end
  end

  class Machine
    attr_reader :name, :classes, :desc, :facts, :parameters

    def initialize(name, dsl, &block)
      @name = name
      @dsl = dsl
      @desc = String.new
      @parameters = Hash.new
      @classes = @dsl.global_classes
      @facts = @dsl.global_facts

      # Run this machine's configuration
      instance_eval(&block) if block
    end

    def regex
      # Build the regexes for this machine type
      # eg, we want the "web-build" machine to match hostnames names like
      # bleanhar-web-build.usersys.redhat.com
      Regexp.new("-%s" % @name)
    end

    def matches?(hostname)
      regex.match(hostname)
    end

    def matching_chars(hostname)
      regex.match(hostname)[0].size rescue 0
    end

    def set_desc(text)
      @desc = text
    end

    def set_classes(*classes)
      @classes.concat(classes).uniq!
    end

    def set_params(params)
      @parameters.merge(params)
    end

    def include_facts(*facts)
      # reject the cases where lookup_fact returns nil
      @facts.concat(facts.map {|f| @dsl.lookup_fact(f)}.compact)
    end
  end
end
