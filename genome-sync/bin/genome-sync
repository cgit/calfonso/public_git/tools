#!/usr/bin/env ruby
require 'open-uri'
require 'fileutils'
require 'git'
require 'main'
require 'highline/import'
require 'genome-sync'
#http://www.mail-archive.com/capistrano@googlegroups.com/msg01822.html
HighLine.track_eof = false

include GenomeSync

Main {
  version Version::STRING
  description "A tool for syncronizing Genome Repo machines."

  # Normally I wouldn't use class variables but I think it's warranted within
  # the 'main' DSL.  If there are any other global config variables feel free 
  # to put them here.
  APP_NAME = "genome-sync"
  DEFAULT_WORKING_DIR = File.join(ENV["HOME"], "." + APP_NAME)

  @@repo_path_prefix = "/pub/git"
  @@puppetmaster_base_dir = "/etc/puppet/modules/main"

  # Main hook
  def handle_exception e
    puts e.message
  end unless $DEBUG

  option('verbose', 'v')

  option('workingdir', 'w') {
      description "The working directory to sync"
      default DEFAULT_WORKING_DIR
      attr
      argument_required
  }

  def run
    require 'rbconfig'
    script = File.join(Config::CONFIG['bindir'], APP_NAME)
    exec("#{script} --help")
  end

  mode('clean') {
    def run() 
      verbose("Removing #{workingdir}")
      FileUtils.rm_rf(workingdir) 
    end
  }

  mode('start') {
    description "Guides the user through the syncronization process"

    option('repo', 'r'){
      description "Genome Repository to syncronize with"
      required
      argument_required
    }

    mode('quick') {
      description "Perform's hard reset to a given Genome Repo." 
 
      def run
        start do |git|
          git_action(git, :non_interactive => true) do |b|
            git.reset_hard(b)
          end
        end
      end
    }

    def run
      start(:interactive => true) do |git|
        # Begin YACW (Yet Another Cheesy Wizard)
        choose do |menu|
          menu.prompt = "What would you like to do?"

          menu.choice "'reset --hard' to the remote" do 
            git_action(git) do |b|
              git.reset_hard(b)
            end
          end

          menu.choice "merge" do
            git_action(git) do |b|
              git.merge(b)
            end
          end

          menu.choice "rebase" do
            git_action(git) do |b|
              git.rebase(b)    
            end
          end

          menu.choice "something even more exciting" do
            git_action(git) do |b|
              fork do
                say("Type 'exit' when you are done to continue")
                git.chdir {exec('bash')}
              end

              Process.wait2
            end
          end
        end
      end
    end
  }

  mode('save') {
    description "Push the changes in the working directory to #{@@repo_path_prefix}.\n" +
                "The puppet modules will have their 'master' branches\n" +
                "checked out to #{@@puppetmaster_base_dir}."

    def run
      working_git_dirs.each do |d|
        repo_base_path = d.sub(workingdir, "") # Chop off the working dir
        dest = File.join(@@repo_path_prefix, repo_base_path)
        bare_repo_name = File.basename(dest)
        bare_repo_parent_dir = File.dirname(dest)

        unless File.directory?(dest)
          verbose("Making #{bare_repo_parent_dir}")
          FileUtils.mkdir_p(bare_repo_parent_dir)

          verbose("Cloning #{d} to #{dest}")

          # Handle the special puppet module case
          if repo_base_path =~ %r-^/?puppet- 

            # For the correct code to make it to the puppetmaster we need to
            # make sure 'master' is the active branch.
            Git.open(d).checkout('master')

            puppetmaster_dir = File.join(@@puppetmaster_base_dir, bare_repo_name)
            verbose("Checking out 'master' at #{puppetmaster_dir}")
            Git.clone(d, puppetmaster_dir)
            # HACK: I couldn't get the git gem to clone the working dir for the
            # puppetmaster and the bare repo for gitweb.  This is a workaround.
            FileUtils.mv(File.join(puppetmaster_dir, ".git"), dest)
          else 
            Git.clone(d, 
                      bare_repo_name, 
                      :path => bare_repo_parent_dir,
                      :bare => true)
          end
        end

        verbose("Pushing all branches in #{d} to #{dest}:")
        git = Git.open(d)
        git.branches.local.each do |b| 
          next if b.name == "(no branch)" #important for superprojects
          b.checkout
          verbose("#{b}")
          git.push(dest,b, :force => true)
        end
        verbose("done.")
      end
    end
  }

  ##############################################################################
  # These methods are available to all modes since their in the global namespace
  ##############################################################################

  def verbose(msg)
    puts msg if params['verbose'].given?
  end
 
  # I couldn't find a way to 'checkout -b --force' so this will have to do for
  # now.  It would be better to work this into the git gem at some point.
  def checkout_b(git, branch, opts = {})
    verbose("Setting #{branch.name} to #{git.object(branch).sha}")
    git.branch(branch.name).checkout
    git.reset_hard(branch) 
  end

  def working_git_dirs
    Dir[workingdir + "/**/.git"].reject do |g|
      g == File.join(workingdir, ".git") # ignore superprojects
    end.map {|g| File.dirname(g)}
  end

  # Just a helper method.  Handle recovery
  def git_action_safe(g, b)
    local_branches = g.branches.local

    # See if the branch already exists
    if lb = local_branches.find {|l| l.name == b.name}
      lb.checkout
      begin
        yield(b)
      rescue Git::GitExecuteError => e
        fork do
          puts e.message
          puts "Type 'exit' when everything is fixed to continue '#{APP_NAME}'"
          g.chdir {exec('bash')}
        end

        Process.wait2
      end
    else # We must create the branch
      checkout_b(g,b)
    end
  end

  # Abstracts the UI for all git modes.  Regardless or whether you are
  # merging, rebasing, etc we want the user to be presented with the same choices.
  # Namely, they can select a branch if there are multiple remote branches and with
  # that branch they can do whatever action is contained in the 'block'--all the while 
  # recovering from git failures. 
  def git_action(g, opts={}, &block)
    remote_branches = g.branches.find_all do |b| 
      (b.remote.to_s == @genome_repo) && b.name != 'HEAD'
    end

    # This is a horrible hack.  The interactiveness should not be this method's
    # concern.
    if opts[:non_interactive] 
      remote_branches.each do |b| 
        git_action_safe(g, b, &block)
      end 
    else
      # Don't bother asking the user for a branch if there is only one
      if remote_branches.size == 1
        git_action_safe(g, remote_branches[0], &block)
      else
        choose do |m| 
          m.prompt = "Select branch."


          m.choices(*remote_branches) do |b|
            git_action_safe(g, b, &block)
          end

          m.choice("all branches") do
            remote_branches.each do |b|
              git_action_safe(g, b, &block)
            end 
          end
        end
      end
    end
  end

  def start(opts={})
    # Setup
    verbose "making #{workingdir}"
    FileUtils.mkdir_p(workingdir)

    @genome_repo = params['repo'].value

    verbose("Finding available repos on #{@genome_repo}")
    open("http://#{@genome_repo}/git/gitweb.cgi?a=project_index") do |f|
      # Sometimes gitweb attaches the owner's name of the repo.  The regex
      # should grab just the part we need.
      @git_repo_paths = f.readlines.map {|line| line.strip.match(/([^\s]*)/)[1]}
    end

    # Iterate over each repo and guide the user through the sync process
    @git_repo_paths.each do |p|
      next unless agree("Sync #{p}? (y/n)", true) if opts[:interactive]

      clone_dir = File.join(workingdir, p)
      verbose "clone_dir = #{clone_dir}"

      # Handle the case where the repo path is foo/bar
      dirs = File.split(p) 
      if dirs.size > 1
        subdirs = dirs[0..-2]
        repo_parent_dir = workingdir + File::SEPARATOR + File.join(subdirs)
        verbose "Making #{repo_parent_dir}"
        FileUtils.mkdir_p(repo_parent_dir)
      end

      repo_path = "#{@@repo_path_prefix}/#{p}"
      repo_url = "git://#{@genome_repo + repo_path}"

      # We need to clone from the remote if the local clone doesn't exist
      git = if File.directory?(clone_dir)
        verbose "Opening #{clone_dir}"
        Git.open(clone_dir) 
      else
        # NOTE: For symplicity's sake I decided not to bother cloning from /pub/git
        # locally if the repo does not exist in the working dir.  This would
        # make the tool have to detect if the local clone is out sync.  While
        # this isn't hard, I don't feel like messing with it at the moment.
        verbose("Cloning #{repo_url} to #{clone_dir}")
        g = Git.clone(repo_url, clone_dir)
    
        g.branches.remote.each do |r| 
          next if r.name == 'HEAD'
          checkout_b(g, r) 
        end
        
        # Don't bother asking the user anything else about this repo if we
        # don't even have a local public copy yet.
        unless File.directory?(repo_path)
          verbose("No local public repo found at #{repo_path}") 
          next 
        end
  
        g # value from if
      end

      ##############################
      # Setup Remotes:
      ##############################
      
      # Don't add the remote if it already exists
      # Note: By convention the remote must be the same as the genome repo fqdn
      git.add_remote(@genome_repo, repo_url) unless git.remotes.find {|r| r.name == @genome_repo}
      verbose("Fetching #{@genome_repo}")
      git.fetch(@genome_repo)

      # If we have a public repo make sure we have a remote that points to it
      localcache_remote_name = 'localcache'
      if File.directory?(repo_path) # This dir will exist if the user has ever used 'save'
        # Add a remote if we don't have one
        unless git.remotes.find {|r| r.name == localcache_remote_name}
          git.add_remote(localcache_remote_name, repo_path) 
        end

        verbose("Fetching #{localcache_remote_name}")
        git.fetch(localcache_remote_name)

        # reset hard all our local branches
        local_public_branches = git.branches.remote.find_all do |b| 
          next if b.name == 'HEAD'
          b.remote.name == localcache_remote_name
        end

        local_public_branches.each {|b| checkout_b(git, b, :force => true)}
      end

      yield(git)
    end
  end
}
