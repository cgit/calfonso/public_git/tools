# Generated from genomed-0.1.0.gem by gem2rpm -*- rpm-spec -*-
%define ruby_sitelib %(ruby -rrbconfig -e "puts Config::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname genomed
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Genome Repository daemon for machine configuration
Name: rubygem-%{gemname}
Version: 1.0.0
Release: 8%{?dist}
Group: Applications/System
License: GPLv2+
URL: http://fedorahosted.org/genome
Source0: %{gemname}-%{version}.gem
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: httpd
Requires: rubygems
Requires: rubygem(picnic) 
Requires: rubygem(activesupport) 
Requires: rubygem(reststop) >= 0.2.1
Requires: rubygem(genome-dsl)
Requires: genome-styling
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
Genome Repository daemon for machine configuration

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir} %{buildroot}/etc/init.d %{buildroot}/etc/genomed
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
mv %{buildroot}%{geminstdir}/extra/genomed.redhat %{buildroot}/etc/init.d/genomed
mv %{buildroot}%{geminstdir}/extra/config.yml %{buildroot}/etc/genomed/config.yml
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

# Put in the apache rules
mkdir -p %{buildroot}/etc/httpd/conf.d
mv %{buildroot}%{geminstdir}/extra/genomed.apache %{buildroot}/etc/httpd/conf.d/genomed.conf

%clean
rm -rf %{buildroot}

%files
%{_bindir}/genomed
%{_bindir}/genomed-ctl
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec
%config /etc/genomed/config.yml
/etc/httpd/conf.d/genomed.conf

%defattr(755, root, root, -)
/etc/init.d/genomed

%changelog
* Mon Jun 23 2008  <bleanhar@redhat.com> - 1.0.0-1
- 1.0 Release
