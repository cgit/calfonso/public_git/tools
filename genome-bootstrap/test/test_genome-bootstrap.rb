# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'genome-bootstrap/ddns'
require 'rubygems'
require 'mocha'

class TestDDNS < Test::Unit::TestCase
  def setup
    @test_main_page = File.read(File.dirname(__FILE__) + "/data/test_main_page.html")
  end

  def mkddns(hostname)
    RedHatDDNS::DDNS.new("fakeuser", "fakepass", hostname)
  end

  def test_exact_match_exists
    ddns = mkddns("bleanhar-jboss-mysql")
    ddns.expects(:main_page).yields(@test_main_page)
    ddns.expects(:request_new_hash).never
    assert_equal("bleanhar-jboss-mysql24c50", ddns.ddns_hash)
  end

  def test_hostnames_in_reverse_alphabetical_order
    ddns = mkddns("qax")
    ddns.expects(:main_page).yields(@test_main_page)
    ddns.expects(:request_new_hash).never
    assert_equal("qax2881b", ddns.ddns_hash)
  end

  def test_exact_match_doesnt_exist_but_a_superset_does
    ddns = mkddns("bleanhar-jboss")
    ddns.stubs(:main_page).yields(@test_main_page)
    ddns.expects(:request_new_hash).times(3) # This is the default number of tries

    assert_raise RuntimeError do
      ddns.ddns_hash
    end
  end
end
