%define ruby_sitelib %(ruby -rrbconfig -e "puts Config::CONFIG['sitelibdir']")
%define gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%define gemname genome-bootstrap
%define geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Tool for provisioning virtual machines
Name: rubygem-%{gemname}

Version: 1.0.0
Release: 9%{?dist}
Group: Development/Languages
License: GPLv2
URL: http://genome.et.redhat.com
Source0: %{gemname}-%{version}.gem
BuildRoot: %{_tmppath}/%{name}-%{version}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(highline) 
Requires: rubygem(reststop) 
Requires: rubygem(activesupport) 
Requires: rubygem(main) 
Requires: wget
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Obsoletes: genome-bootstrap
Provides: genome-bootstrap

%description
Tool for bootstrapping the puppet configuration on machine images


%prep

%build

%install
%{__rm} -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
cp %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rm -rf %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root)
%{_bindir}/genome-bootstrap
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%changelog
* Tue Jul 22 2008 Brenton Leanhardt <bleanhar@redhat.com> - 1.0.0-9
- Fixed license
* Tue Jul 22 2008 Jeroen van Meeuwen <kanarip@fedoraproject.org> - 1.0.0-8
- Initial build
