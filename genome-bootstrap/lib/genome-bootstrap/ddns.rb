# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'net/https'

module RedHatDDNS
  SERVER = 'kickstart.rdu.redhat.com'

  class DDNS
    def initialize(username, password, host)
      @username = username
      @password = password
      @hostname = host
    end
  
    def ddns_hash
      @ddns_hash ||= fetch_hash
    end
   
    def to_s
      ddns_hash
    end

    def main_page
      req = "wget -q -O- --no-check-certificate " +
            "--http-user=#{@username} --http-password='#{@password}' " +
            %["https://#{SERVER}/redhat-ddns/admin/"]

      puts "Fetching DDNS hash..."
      yield `#{req}`
    end
 
    def request_new_hash
      req = "wget -q -O- --no-check-certificate " +
            "--http-user=#{@username} --http-password='#{@password}' " +
            %["https://#{SERVER}/redhat-ddns/admin/add.php?host=#{@hostname}&_submit=Submit+Request"]
    
      puts "Requesting a new hash"
      `#{req}` 
    end

    def fetch_hash(options={:tries => 3})
      options[:tries].times do 
        # In order to test the regex with mocha we can't rely on the 
        # return value from the main_page method.  That's why I 
        # declare 'hash' here and store the results from inside the 
        # block
        hash = nil 
        main_page do |html|
          hash = /#{@hostname}<\/pre>.*?hash=(.*)\"/.match(html)[1] rescue nil
        end
        return hash if hash
        puts "Hash not found for #{@hostname}"
        request_new_hash
      end

      raise RuntimeError.new("Error fetching DDNS hash:\n" +
                             "This usually happens when a hostname is reserved under another " +
                             "user's account or you typed password incorrectly.")
    end
  end
end
