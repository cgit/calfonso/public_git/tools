Summary: Genome bootstrapping service
Name: genome-firstboot
Source: genome-firstboot.tar.gz
Version: 1.0.0
Release: 1%{?dist}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Group: RHWWW/Development
Vendor: Red Hat
URL: http://clearspace.knowledge.str.redhat.com/docs/DOC-1456
License: GPL
BuildArch: noarch

%description
Genome firstboot-like bootstrapping service

%prep

%setup -c

%build

%install
# Cleaning up the build root
rm -rf $RPM_BUILD_ROOT

# Create the directory structure required to lay down our files
mkdir -p $RPM_BUILD_ROOT/etc/sysconfig $RPM_BUILD_ROOT/etc/init.d $RPM_BUILD_ROOT/usr/sbin

cp etc/init.d/genome-firstboot $RPM_BUILD_ROOT/etc/init.d
cp usr/sbin/genome-firstboot $RPM_BUILD_ROOT/usr/sbin

# This gets set to yes by another genome tool
echo "RUN_BOOTSTRAP=NO" > $RPM_BUILD_ROOT/etc/sysconfig/genome-firstboot

%clean
rm -rf $RPM_BUILD_ROOT

%preun
if [ "$1" = "0" ] ; then # last uninstall
    /sbin/chkconfig genome-firstboot off
fi

%post
if [ "$1" = "1" ] ; then  # first install
  /sbin/chkconfig genome-firstboot on
fi

%files
%defattr(-,root,root,-)
%config /etc/sysconfig/genome-firstboot

%defattr(744,root,root,-)
/etc/init.d/genome-firstboot
/usr/sbin/genome-firstboot
