Summary: Genome CSS Styles
Name: genome-styling
Source: genome-styling-bin.tar.gz
Version: 1.0.0
Release: 4%{?dist}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Group: Applications/System
URL: https://fedorahosting.org/genome
License: GPLv2
Requires: httpd

%description
Genome CSS Styles

%prep

%setup -c

%build

%install
# Cleaning up the build root
rm -rf $RPM_BUILD_ROOT

# Create the directory structure required to lay down our files
mkdir -p $RPM_BUILD_ROOT/var/www/html

cp -R . $RPM_BUILD_ROOT/var/www/html

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/var/www/html/styles.css
/var/www/html/genome-mainbgcurve.png
/var/www/html/genome-mainbg.png
/var/www/html/glogo.gif

%doc
