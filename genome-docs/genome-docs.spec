Name:           genome-docs
Version:        1.0.0
Release:        38%{?dist}
Summary:        Genome documentation 

Group:          Applications/System
License:        GPL 
URL:            http://genome.et.redhat.com
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  publican
Requires:       httpd

%description
Genome publican documentation

%prep
%setup -c 0

%define brand	genome

%build
# First build and install the genome theme
pushd publican-genome
make Common_Content
rm -rf $RPM_BUILD_ROOT
mkdir -p -m755 $RPM_BUILD_ROOT%{_datadir}/publican/Templates
mkdir -p -m755 $RPM_BUILD_ROOT%{_datadir}/publican/make
mkdir -p -m755 $RPM_BUILD_ROOT%{_datadir}/publican/xsl/%{brand}
cp -rf Common_Content $RPM_BUILD_ROOT%{_datadir}/publican/
cp -rf Book_Template $RPM_BUILD_ROOT%{_datadir}/publican/Templates/%{brand}-Book_Template
cp -rf Set_Template $RPM_BUILD_ROOT%{_datadir}/publican/Templates/%{brand}-Set_Template
cp -rf Article_Template $RPM_BUILD_ROOT%{_datadir}/publican/Templates/%{brand}-Article_Template
install -m 755 make/Makefile.%{brand} $RPM_BUILD_ROOT%{_datadir}/publican/make/.
install -m 755 xsl/*.xsl $RPM_BUILD_ROOT%{_datadir}/publican/xsl/%{brand}/.
popd

# Now build the genome-documentation
make clean html html-single

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/pub/docs/genome
mkdir -p $RPM_BUILD_ROOT/etc/httpd/conf.d

cp -r tmp/* $RPM_BUILD_ROOT/pub/docs/genome
cp docs.conf $RPM_BUILD_ROOT/etc/httpd/conf.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/etc/httpd/conf.d/docs.conf
/pub/docs

%doc

%changelog
