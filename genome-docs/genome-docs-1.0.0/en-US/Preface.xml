<?xml version='1.0'?>
<!DOCTYPE preface PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>

<preface id="genome-Preface">
	<title>Preface</title>

	<para>
		The genome is fundamental to the reliable encoding and transfer
		of both genetic code and data.  As the project name suggests,
		&PRODUCT; is the equivalence for software systems.  The project
		started formally in early 2008, though the origins can be traced
		back several years prior to real struggles within Red Hat IT
		developing and deploying software.
	</para>

	<para>
		While it may not be the perfect analogy, it is indeed fitting
		to say that <emphasis>hereditary</emphasis> information is
		stored within every IT organization.  The truth is software
		systems, like species, face extinction through poor replication
		of this information. Sadly, the knowledge that is required to
		maintain and reproduce complex systems often only lives in the
		form of tangled configuration scripts or, worse still, only in
		the minds of consulting domain experts.  Transfering knowledge
		in such manners is practically a recipe for building legacy
		systems.  
	</para>	

	<para>
		Taking the biological analogy a little further, briefly imagine
		a world in which generations of genetic information had to be
		manually replicated by <emphasis>any</emphasis> number of
		people.  Now try to imagine a different world in which genetic
		information could only be copied <emphasis>exactly</emphasis>,
		that is to say, diversity is altogether unattainable.
		&PRODUCT; aims to solve both of these problems for IT; that of
		reproducing exceedingly complicated systems in a world where
		heterogeneity is <emphasis>always</emphasis> more of the rule
		than the exception.
	</para>
		
	<para>
		As you begin tackling these problems for your organization
		it cannot be emphasized enough that the collaboration amongst
		teams enabled by &PRODUCT; is more important than any
		particular tool implementation.  Feel free to
		<emphasis>mutate</emphasis> &PRODUCT; into any shape or form to
		solve your problems.  The truth is, we readily await your patches
		and enjoy seeing the best ideas rise to the top.  
	</para>

	<xi:include href="Common_Content/Conventions.xml" xmlns:xi="http://www.w3.org/2001/XInclude" />
	<xi:include href="Feedback.xml" xmlns:xi="http://www.w3.org/2001/XInclude">
		<xi:fallback xmlns:xi="http://www.w3.org/2001/XInclude">
			<xi:include href="Common_Content/Feedback.xml" xmlns:xi="http://www.w3.org/2001/XInclude" />
		</xi:fallback>
	</xi:include>
</preface>
