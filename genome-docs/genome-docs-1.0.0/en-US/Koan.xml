<?xml version='1.0'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>

<chapter id="genome-Koan">
	<title>Provisioning with Koan</title>

	<section id="genome-Koan-Background">
		<title>Background</title> 
		<para> Koan is a tool coming out of Red Hat Emerging
			Technologies that is used to provision machines from
			Cobbler servers. Following the unix philosophy it's
			very simple to use and the man page will tell you
			everything you need to know. For more information check
			out the <ulink
				url="http://cobbler.et.redhat.com/documentation.php">cobbler
				documentation</ulink>.  
		</para>

		<note>
			<title>Note</title>
			<para>
				Most provisioning with the &PRODUCT; tools
				can be done without having to work with Koan
				directly.  However, a good understanding of its
				basic operation is useful for advanced usage of
				the &PRODUCT; tooling.
			</para>
		</note>
	</section>

	<section id="genome-Koan-Installation">
		<title>Installation</title> 
		<para> RPMs exist for both Fedora and RHEL (through <ulink
				url="http://fedoraproject.org/wiki/EPEL">EPEL</ulink>).
			If your repositories are configured correctly you should simply
			be able to <userinput>yum install koan</userinput>. Koan doesn't have many
			dependencies so if you don't feel like adding the EPEL repo to
			your RHEL machine you can simply install the RPM.  
		</para>

		<para> Once installed you should test your installation against a
			cobbler server.  
			<screen>
koan -s genome-repo.usersys.redhat.com --list=profiles
koan -s genome-repo.usersys.redhat.com --list=systems
			</screen> 
		</para>
	</section>


	<section id="genome-Koan-GuestProvisioning">
		<title>Guest Provisioning</title>
		<note>
			<title>Note</title>
			<para>
				<application>genome-bootstrap</application>
now wraps <application>Koan</application> for provisioning virtual machines.
This is only included for advanced use cases.  
			</para>
		</note>
		<screen> 
koan -s genome-repo.usersys.redhat.com --virt --virt-type=xenpv --virt-path=HostVolGroup00 --system=[your hostname]
		</screen>

		<para>
			Here the most important part is obviously the
			<emphasis>--virt</emphasis> flag. If you pass in a
			Volume Group name for <emphasis>--virt-path</emphasis>
			koan will automatically create (or reuse) a logical
			volume in the format of
			<literal>[name]-disk0</literal>. With cobbler much of the
			configuration lies on the server side (the memory, size
			of the logical volume, etc). If you have different
			requirements you can either create a new profile for
			cobbler or you can use the <link
				linkend="genome-Tooling">tooling</link> that
			makes up &PRODUCT; achieve the desired results. 
		</para>

		<tip>
			<title>Tip</title>
			<para>
				One trick to creating a quest with a larger logical
				volume that a cobbler profile specifies is to simply
				create it by hand and specify the size you desire. Koan
				will simply reuse that logical volume.	
			</para>
		</tip>
	</section>
	
	<section>
		<title>Watching the VM</title>
		<para>
			During the kickstart provisioning process you can
			connect to the virtual framebuffer which is accessible
			through VNC. It's only available locally so don't try
			and connect from another machine. From the Xen host you
			should be able to use:
			<screen> 
ssh -X root@YourXenHost.usersys.redhat.com
vncviewer localhost:5900
			</screen>
		</para>

		<para>
			The port may vary according to how many guests you have
			running. To find out which ports are being used:
			<screen> 
# If you are using RHEL5 less than U2
netstat -nlp | grep vnc

#otherwise
netstat -nlp | grep qemu-dm
			</screen>
		</para>
	</section>

	<section>
		<title>Cleaning Up</title>
		<para>
			If you would like to remove work performed by koan:
			<itemizedlist>
				<listitem>
					<para>
						Remove the Xen configuration
						for the guest under
						<filename>/etc/xen</filename>
					</para>
				</listitem>
				<listitem>
					<para>
						Remove the file or logical
						volume that backs your guest.
					</para>
				</listitem>
			</itemizedlist>
		</para>
	</section>

	<section>
		<title>Known Issues</title>
		<para>
			Provisioning will fail if a config file under
			<filename>/etc/xen</filename> has the same name as the
			machine you are trying to create.  The error message is
			fairly cryptic and says something like "machine already
			exists".  The fix is to simply remove the config file.
		</para>
	</section>

</chapter>

