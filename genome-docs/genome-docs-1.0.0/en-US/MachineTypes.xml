<?xml version='1.0'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>

<chapter id="genome-Machines">
	<title>Genome Appliances</title>

	<section id="genome-appliances">
		<title>Appliances</title>
		<para>
			Appliances in the &PRODUCT; environment are machines
			that enable the &PRODUCT; tooling.
		</para>

		<section id="genome-cloud-appliance">
			<title>Cloud Appliance</title>
			<para>
				The Cloud Appliance is used to host Xen
				guests in the &PRODUCT; environment.  
            </para>
            <section id="genome-cloud-appliance-systemreqs">
                <title>System Requirements</title>
                <variablelist>
                    <varlistentry>
                        <term>CPU</term>
                        <listitem><simpara>1GHz</simpara></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>Memory</term>
                        <listitem><simpara>This depends on the number of cloud members you plan on hosting.  We recommend 1.5G of RAM to start.</simpara></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>System architecture</term>
                        <listitem><simpara>A cloud appliance can be installed on either i386 or x86_64 architectures.</simpara></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>Hardware Virtualization</term>
                        <listitem><simpara>When using Fedora as the distribution for the cloud, the machine must support hardware virtualization.</simpara></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>Storage</term>
                        <listitem><simpara>This depends on how many cloud members you plan on hosting.  We recommend 100G of hard drive space to start.</simpara></listitem>
                    </varlistentry>
                </variablelist>
                <important><simpara>Cloud Appliances should only be installed on a Physical Machine and not a Virtual Machine</simpara></important>
            </section>
            <section>
                <title>Features</title>
                <itemizedlist>
                    <listitem><para>Virtualization (Either Xen or KVM)</para></listitem>
                    <listitem><para>&PRODUCT; tooling for managing the cloud</para></listitem>
                </itemizedlist>
            </section>
        </section>
		<section id="genome-appliance">
			<title>Genome Appliance</title>
			<para>
				The Genome Appliance is the center of development in the
				&PRODUCT; environment.  In a nutshell it is the
				self-contained provisioning, configuration and artifact
				store.  For this reason Genome Appliances are
				generally not considered volatile. 
			</para>

			<para>
				As all other machine types it is designed to work as
				both a "baremetal" and virtual machine.  The main
				resource requirement that distinguishes this machine
				type is disk space, which is a function of the amount of
				bits imported to <application>cobbler</application>.
				<note>
					<title>Note</title>
					<para>
						There was a time when Genome
						Appliances were able to be
						created via <application><link
								linkend="genome-GenomeBootstrap">genome-bootstrap</link>
						</application>.  This led to
						several "chicken and the egg"
						sorts of problems.  For this
						reason the method for
						provisioning Genome Appliances was
						switched to RPM.
					</para>
				</note>
            </para>


            <section id="genome-appliance-systemreqs">
                <title>Minimum System Requirements</title>
                <variablelist>
                    <varlistentry>
                        <term>CPU</term>
                        <listitem><simpara>1GHz</simpara></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>Memory</term>
                        <listitem><simpara>512M RAM</simpara></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>System architecture</term>
                        <listitem>
                            <simpara>
                                A Genome Appliance can be installed on either i386 or x86_64 architectures.
                            </simpara>
                        </listitem>
                    </varlistentry>
                    <varlistentry>
                        <term>Storage</term>
                        <listitem>
                            <simpara>
                                This depends on how many distros you plan on hosting in cobbler.  We recommend 50G of hard drive space to start.
                            </simpara>
                        </listitem>
                    </varlistentry>
                </variablelist>
            </section>

			<section>
				<title>Features</title>
				<para>
					<itemizedlist>
						<listitem>
							<para>
								Cobbler for all RPM/provisioning
							</para>
						</listitem>
						<listitem>
							<para>
								A Puppetmaster for all configuration
							</para>
						</listitem>
						<listitem>
							<para>
								Bare git repos
								for all content
								located under
								/pub/git
							</para>
						</listitem>
						<listitem>
							<para>
								GitWeb running
								on
								http://[hostname]/git/gitweb.cgi
							</para>
						</listitem>
						<listitem>
							<para>
								Genomed
								running
								http://[hostname]:8106/nodes.html
							</para>
						</listitem>
					</itemizedlist>
				</para>
			</section>

			<section>
				<title>Genome Appliance cloning</title>
				<para>
					The state of a particular Genome Appliance can be
					described by the content stored under
					/var/www/cobbler and /pub/git.  Cloning a
					particular Genome Appliance is really just a matter of
					getting the correct bits from those locations
					onto a new Genome Appliance.  
				</para>

				<para>
					Aside from the simple bit replicatation that
					must be performed there are also a few
					"one-off" things that need to happen.  This
					involves:
					<itemizedlist>
						<listitem>
							<para>
								Getting the
								puppet modules
								to the location
								where the
								<application>puppetmaster</application>
								can see them.  
							</para>
						</listitem>
						<listitem>
							<para>
								Setting up
								commit hooks
								for the puppet
								module git
								repositories.
							</para>
						</listitem>
						<listitem>
							<para>
								Sets up commit
								hook for the
								&PRODUCT;
								documentation.
							</para>
						</listitem>
					</itemizedlist>
				</para>

				<para>
					See the <link
						linkend="genome-RepoMachineBootstrapping">cookbook</link>
					for more information.
				</para>
			</section>

			<section id="genome-repo-machine-customization">
				<title>Genome Appliance customization</title>
				<para>
					The &REPORPM; is designed to get users up and
					running with a known working configuration.
					There are certain custom settings users of
					&PRODUCT; will need to configure for their
					environment.  The two most common needs for
					customization are adding new Genome machine
					types to <link
						linkend="genomed-configuration">genomed</link>
					and any extra
					<application>cobbler</application>
					customization.
				</para>

				<para>
					How these customizations are managed is at the
					user's discretion. However, since the Repo
					machine is already controlled by
					<application>puppet</application> it makes
					sense in many cases to simply use it for this
					as well.  
				</para>

				<para> 
					For this to work a puppet module named
					<emphasis>repo_extensions</emphasis>
					must be created and exist on the module
					path.   The class that this module
					must define is also called
					<emphasis>repo_extensions</emphasis>.

					<important>
						<title>Important</title>
						<para>
							The reason this works
							is because by default
							the Genome Appliance's
							<application>puppet</application>
							external nodes script
							includes two classes:
							<emphasis>genomerepo::appliance</emphasis>
							and
							<emphasis>repo_extensions</emphasis>.
						</para>
					</important>
				</para>
			</section>
		</section>
	</section>

	<section id="genome-custom-types">
		<title>Custom Machine Types</title>
		<para>
			A custom machine type in the &PRODUCT; environment can
			be roughly described as a collection of known working
			<application>puppet</application> classes matched with
			an operating system (or more precisely, a
			<application>cobbler</application> profile). The list
			of machines that can be provisioned from a given Genome
			Appliance can be found when using the <link
				linkend="genome-GenomeBootstrap">genome-bootstrap</link>
			wizard or the <link
				linkend="genome-Genomed">genomed</link> UI.  
	
			<note>
				<title>Note</title>
				<para>
					See the <link
						linkend="genome-AddMachineType">cookbook</link>
					for more information on
					creating custom machine types.
				</para>
			</note>

			<important>
				<title>Important</title>
				<para>
					From Puppet's point of view these "types" are
					not bound to any particular OS version.  You
					choose the OS with <link
						linkend="genome-GenomeBootstrap">
						genome-bootstrap</link> or when <link
						linkend="genome-Koan">provisioning
						directly with Koan</link>. This allows
					users to test out different OS and applications
					versions using the same Puppet code.
				</para>
			</important>
		</para>
	</section>
</chapter>
