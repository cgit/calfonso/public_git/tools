<?xml version='1.0'?>
<!DOCTYPE appendix PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>

<appendix id="genome-Revision_History">
	<appendixinfo>
		<xi:include href="Revision_History.xml" xmlns:xi="http://www.w3.org/2001/XInclude" />
	</appendixinfo>
	<title>Revision History</title>

	<section id="genome-LoggingIn"> 
		<title>Logging in to &PRODUCT; machines</title>
		<para>
			The only interesting thing about logging into &PRODUCT;
			machines is the root password.  It is currently set in
			the kickstart file in our Cobbler profiles.  That means
			if you do any provisioning with Koan in the &PRODUCT;
			environment your root password will be
			<emphasis>&GENOMEPASSWORD;</emphasis>.  Users can
			change the password to anything they like once logged
			in.
		</para>
	</section>

	<section id="genome-release"> 
		<title>Managing releases with the &PRODUCT; tooling</title>
		<para>
			One of the challenges of working on large teams is
			simply keeping track of all the various forms of
			content that make up a project.  While teams have
			traditionally used some sort of Source Code Management
			tool such as <application>subversion</application> or
			<application>git</application> the same discipline also
			applies to configuration artifacts and binary
			dependencies.
		</para>

		<para>
			For this reason, projects making use of the &PRODUCT;
			tooling have the ability to track all content via
			<application>git</application> repositories.  Detailed
			below is a process that handles bundling the state of
			several <application>puppet</application> modules, RPM
			dependencies and source code into one deliverable that
			can be tracked throughout the project lifecycle.
		</para>

		<section id="genome-release-repo">
			<title>The "release" repository</title>
			<para>
				The release <application>git</application>
				repository is basically just a
				<ulink
					url="http://git.or.cz/gitwiki/GitSubmoduleTutorial"><emphasis>superproject</emphasis>
				</ulink>
				 which can
				contain any number of
				<emphasis>submodules</emphasis>.  This allows
				project dependencies to be woven together as
				needed.
			</para>
		</section>

		<section id="genome-release-repo-usage">
			<title>Creating a superproject</title>
			<para>
				A superproject is really just a normal git
				repository for tracking the states of other
				repositories.  
				<screen>
# Create a new git repository
mkdir release
cd release
git init
				</screen>

				Once the repository has been created submodules can be added.

				<screen>
# Add the submodule
git submodule add [url] [path/in/superproject/to/be/created]

				</screen>

				At this point a new file will have been added
				called <filename>.gitmodules</filename>.  This
				is the submodule configuration file.  Another
				"file" that is created is a pseudofile that
				corresponds to the path created when the
				submodule was added.  Both of these should be
				committed.

				<important>
					<title>Important</title>
					<para>
						The url used when adding the
						submodule can be relative.
						This is often more desirable
						than hard coding the path to a
						specific repository.  The main
						reason is that the content
						referenced by a particular
						release repository should
						actually exist in the Repo
						Appliance.  This is a best
						practice that allows Repo
						Appliance state to be backed up
						with the guarantee that a
						project's state can be rebuilt
						and the machines involved can
						be provisioned.  
						See the
						<emphasis>git-submodule</emphasis>
						manpage for more information.
					</para>
				</important>

				<note>
					<title>Note</title>
					<para>
						See the
						<emphasis>git-submodule</emphasis>
						manpage for more information.
					</para>
				</note>
			</para>
		</section>

		<section>
			<title>A word on pushing superprojects</title>
			<para>
				Typically only metadata is stored in the
				release superproject.  For this reason copying
				release deliverables from one Repo Appliance to
				another is not as simple as using
				<userinput>git push</userinput> on only the
				release repository.  If relative submodule
				paths are used (and they should be) the state
				referenced in all submodules must exist on a
				given Repo Appliance.  Luckily, this is quite
				easy to do with <link
					linkend="genome-sync">genome-sync</link>.
			</para>

		</section>

		<section>
			<title>Branching strategy</title>
			<para>
			      Complexity, risk as well as external factors all
			      play a large role in how a particular project
			      decides to branch.  Conventions go a long way to
			      simplifying this process and can make projects
			      move smoothly from development to production.
			</para>

			<para>
				In a nutshell it conventions are:
				<itemizedlist>
					<listitem>
						<para>
							If a project is named
							<emphasis>foo</emphasis>
							then there will be a
							branch called
							<emphasis>foo</emphasis>
							on all git repositories
							touched by that project.
						</para>
					</listitem>
					<listitem>
						<para>
							Branches that match the
							project name are
							considered to be stable
							and "on their way to
							production".  
						</para>
					</listitem>
					<listitem>
						<para>
							Using the release
							superproject is simply a
							matter of wiring up the
							branches for a
							particular project into
							one branch, which also
							bears the name of the
							project.
						</para>

						<para>
							In practice what this
							equates to is, after
							adding the submodules
							to a superproject,
							going into the
							submodule's directory
							and getting the working
							directory to match the
							desired state.  If the
							project branch naming
							conventions are being
							followed the content
							can simply be fetched
							and then checked out.  

						</para>

						<para>
							If the fetch/checkout
							process results in a
							change, at the root of
							the superproject
							<userinput>git
								status</userinput>
							will reflect the
							change.  The changes
							can then been commited
							(on the superproject
							branch that corresponds
							to the project name).
						</para>
					</listitem>
				</itemizedlist>
			</para>

			<note>
				<title>Note</title>
				<para>
					These conventions only
					<emphasis>need</emphasis> to be
					followed at the by the people who are
					"interfaces" between teams.  The use of
					Repo Appliances can also aid the
					branching strategy in that it allows
					each group to determine what works best
					for them.  For example, development and
					release engineering (RE) teams have
					different goals when it comes to
					managing a codebase.  In development a
					team will be more concerned with how to
					balance the bugfix and feature streams
					of a project while RE will focus more
					on how moving these changes through
					the various environments affects other
					projects.
				</para>
			</note>
		</section>

		<section>
			<title>What about the master branch?</title>
			<para>
				For most git repositories it really isn't even
				needed and only aids to confusion since there
				is no consensus as to how branches like
				<emphasis>trunk</emphasis> and
				<emphasis>master</emphasis> should be used.
				The main exception with the &PRODUCT; toolings
				is the case of the
				<application>puppet</application> module
				repositories.  The hook that checks out the
				module code and puts it on the modulepath needs
				to know the name of a particular branch to work
				with.  That branch is the
				<emphasis>master</emphasis> branch.
			</para>
			<para>
				The normal workflow for a
				<application>puppet</application>
				module is to test changes on the
				<emphasis>master</emphasis> branch and
				then push changes to the project branch
				when they are baked.   
			</para>
			<important>
				<title>Important</title>
				<para>
					This process can be followed regardless
					of where in the lifecycle the change
					occurs.  Development can test their
					changes, push to their project branch
					and then QA can push the project branch
					into their master.  Once through QA,
					the code can again be pushed to a
					project branch where RE can take over.
				</para>
			</important>
		</section>
	</section>
</appendix>
