<?xml version='1.0'?>
<!DOCTYPE section PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % RH_ENTITIES SYSTEM "../Common_Content/Entities.ent">
%RH_ENTITIES;
<!ENTITY % RH_TRANS_ENTITIES SYSTEM "../Common_Content/Translatable-Entities.ent">
%RH_TRANS_ENTITIES;
]>

<section id="Document_Conventions">
	<title>രേഖാ കീഴ്‌വഴക്കങ്ങള്‍</title>
	<para>
		ഈ രേഖയിലുള്ള ചില വാക്കുകള്‍ പ്റത്യേക ലിപിയിലും ശൈലിയിലും രൂപത്തിലുമാണ് എഴുതിയിരിക്കുന്നത്. ഇവ പ്രത്യേക വിഭാഗത്തില്‍ പെട്ട വാക്കുകളാകുന്നു. താഴെ പറഞ്ഞിരിക്കുന്നവയാണ് വിഭാഗങ്ങള്‍:
	</para>
	<variablelist>
		<varlistentry>
			<term><literal>കൊരിയര്‍ ലിപി</literal></term>
			<listitem>
				<para>
					<command>നിര്‍ദ്ദേശങ്ങള്‍ക്കും</command>, <filename>ഫയലിന്റെ പേരുകള്‍ക്കും പാഥുകള്‍ക്കും</filename>, <prompt>പ്റോപ്റ്റുകള്‍ക്കും</prompt> കൊരിയറ്‍ ലിപി ഉപയോഗിക്കുന്നു.
				</para>
				<para>
					When shown as below, it indicates computer output: 
<screen>Desktop       about.html       logs      paulwesterberg.png
Mail          backupfiles      mail      reports
</screen>
				</para>
			</listitem>
		</varlistentry>
		<varlistentry>
			<term><userinput>ബോള്‍ഡ് കൊരിയറ്‍ ലിപി</userinput></term>
			<listitem>
				<para>
					നിങ്ങള്‍ ടൈപ്പ് ചെയ്യേണ്ട വാചകങ്ങള്‍ക്കായി ബോള്‍ഡ് കൊരിയറ്‍ ലിപി ഉപയോഗിക്കുന്നു, ഉദാ: <userinput>service jonas start</userinput>
				</para>
				<para>
					നിങ്ങള്‍ക്ക് റൂട്ടായി നിന്ന് ഏതെങ്കിലും നിറ്‍ദ്ദേശം പ്റവറ്‍ത്തിപ്പിക്കണമെങ്കില്‍, നിറ്‍ദ്ദേശങ്ങള്‍ക്ക് മുമ്പേ റൂട്ട് പ്റോപ്റ്റ് (<literal>#</literal>) ഉണ്ടാകുന്നതാണ്:
				</para>
<screen># <userinput>gconftool-2 </userinput>

</screen>
			</listitem>
		</varlistentry>
		<varlistentry>
			<term><replaceable>ഇറ്റാലിക് കൊരിയറ്‍ ലിപി</replaceable></term>
			<listitem>
				<para>
					Italic Courier font represents a variable, such as an installation directory: <filename><replaceable>install_dir</replaceable>/bin/</filename>
				</para>
			</listitem>
		</varlistentry>
		<varlistentry>
			<term><application>ബോള്‍ഡ് ലിപി</application></term>
			<listitem>
				<para>
					<application>പ്റയോഗങ്ങള്‍ക്കുള്ള പ്റോഗ്രാമിനും</application><guilabel>ഗ്രാഫിക്കല്‍ ഇന്ററ്‍ഫെയിസിലുള്ള വാചകങ്ങള്‍ക്കും</guilabel> ബോള്‍ഡ് ലിപി ഉപയോഗിക്കുന്നു.
				</para>
				<para>
					ഇങ്ങനെ ലഭ്യമായാല്‍: <guibutton> ശരി </guibutton>, ഇത് ഗ്രാഫിക്കല്‍ പ്റയോഗത്തിനുള്ള ഇന്ററ്‍ഫെയിസിലുള്ള ബട്ടണ്‍ എന്ന് സൂചിപ്പിക്കുന്നു.
				</para>
			</listitem>
		</varlistentry>
	</variablelist>
	<para>
		ഇവയ്ക്ക് പുറമേ, നിങ്ങളുടെ ശ്രദ്ധ പിടിച്ചുപറ്റുന്നതിനായി പല പ്റയോഗങ്ങളും ഈ മാനുവലില്‍ ഉപയോഗിച്ചിരിക്കുന്നു. അവയുടെ പ്റധാന്യത്തിന്റെ ക്റമത്തില്‍, താഴെ വിശദീകരിച്ചിരിക്കുന്നു:
	</para>
	<note>
		<title>കുറിപ്പ്</title>
		<para>
			സിസ്റ്റമിന്റെ പ്റവറ്‍ത്തനങ്ങള്‍ മനസ്സിലാക്കുന്നതിനായി നിങ്ങളെ ഒരു കുറിപ്പ് സഹായിക്കുന്നു.
		</para>
	</note>
	<tip>
		<title>Tip</title>
		<para>
			ഒരു ജോലി ശരിയായി ചെയ്യുന്നതിന് നിങ്ങളെ സൂചന സഹായിക്കുന്നു.
		</para>
	</tip>
	<important>
		<title>പ്റധാനപ്പെട്ടത്</title>
		<para>
			പ്റധാപ്പെട്ട വിവരങ്ങള്‍ അത്യാവശ്യമാണ്, പക്ഷേ അവ പലപ്പോഴും അപ്റതീക്ഷിതവുമാണ്. ഉദാഹരണത്തിന്, റീബൂട്ടിന് ശേഷം നിലനില്‍ക്കാത്ത ക്റമികരണത്തില്‍ വരുത്തിയിരിക്കുന്ന മാറ്റം.
		</para>
	</important>
	<caution>
		<title>ശ്റദ്ധിക്കുക</title>
		<para>
			നിങ്ങള്‍ക്ക് പിന്തുണ ഏകുന്ന കരാറുകള്‍ക്ക് എതിരെയുള്ള പ്റവറ്‍ത്തികള്‍ സൂചിപ്പിക്കുന്നതിനായി നിങ്ങളെ &#39;ശ്രദ്ധിക്കുക&#39; എന്നത് സഹായിക്കുന്നു. ഉദാഹരണത്തിന്, കേര്‍ണല്‍ വീണ്ടും കംപൈല്‍ ചെയ്യുക.
		</para>
	</caution>
	<warning>
		<title>മുന്നറിയിപ്പ്</title>
		<para>
			ഏറ്റവും അധികം പ്റകടനത്തിനായി ഒരു ഹാര്‍ഡ്‌വെയറ്‍ ഉപയോഗിക്കുമ്പോള്‍ സംഭവിക്കുവാന്‍ സാധ്യതയുള്ള ഡേറ്റാ നഷ്ടങ്ങള്‍ക്കായി മുന്നറിയിപ്പ് സഹായിക്കുന്നു.
		</para>
	</warning>
</section>


