<?xml version="1.0"?>
<!--
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<!-- $Id: fop.xconf 447325 2006-09-18 10:55:33 +0200 (Mon, 18 Sep 2006) jeremias $ -->

<!--

This is an example configuration file for FOP.
This file contains the same settings as the default values
and will have no effect if used unchanged.

Relative config url's will be resolved relative to
the location of this file.

-->

<!-- NOTE: This is the version of the configuration -->
<fop version="1.0">

  <!-- Base URL for resolving relative URLs -->
  <base>.</base>
  
  <!-- Source resolution in dpi (dots/pixels per inch) for determining the size of pixels in SVG and bitmap images, default: 72dpi -->
  <source-resolution>72</source-resolution>
  <!-- Target resolution in dpi (dots/pixels per inch) for specifying the target resolution for generated bitmaps, default: 72dpi -->
  <target-resolution>72</target-resolution>
  
  <!-- Default page-height and page-width, in case
       value is specified as auto -->
  <default-page-settings height="240mm" width="120mm"/>
  
  <!-- Information for specific renderers -->
  <!-- Uses renderer mime type for renderers -->
  <renderers>
    <renderer mime="application/pdf">
      <filterList>
        <!-- provides compression using zlib flate (default is on) -->
        <value>flate</value>
  
        <!-- encodes binary data into printable ascii characters (default off)
             This provides about a 4:5 expansion of data size -->
        <!-- <value>ascii-85</value> -->
  
        <!-- encodes binary data with hex representation (default off)
             This filter is not recommended as it doubles the data size -->
        <!-- <value>ascii-hex</value> -->
      </filterList>

      <fonts>
        <!-- embedded fonts -->
        <!--
        This information must exactly match the font specified
        in the fo file. Otherwise it will use a default font.

        For example,
        <fo:inline font-family="Arial" font-weight="bold" font-style="normal">
            Arial-normal-normal font
        </fo:inline>
        for the font triplet specified by:
        <font-triplet name="Arial" style="normal" weight="bold"/>

        If you do not want to embed the font in the pdf document
        then do not include the "embed-url" attribute.
        The font will be needed where the document is viewed
        for it to be displayed properly.

        possible styles: normal | italic | oblique | backslant
        possible weights: normal | bold | 100 | 200 | 300 | 400
                          | 500 | 600 | 700 | 800 | 900
        (normal = 400, bold = 700)
        -->

        <!--
        <font metrics-url="arial.xml" kerning="yes" embed-url="arial.ttf">
          <font-triplet name="Arial" style="normal" weight="normal"/>
          <font-triplet name="ArialMT" style="normal" weight="normal"/>
        </font>
        <font metrics-url="arialb.xml" kerning="yes" embed-url="arialb.ttf">
          <font-triplet name="Arial" style="normal" weight="bold"/>
          <font-triplet name="ArialMT" style="normal" weight="bold"/>
        </font>
        -->
        <!--font metrics-url="/usr/share/publican/fop/font-metrics-0.93/kochi-mincho-subst.xml"
                kerning="yes" embed-url="/usr/share/fonts/ja/TrueType/kochi-mincho-subst.ttf">
            <font-triplet name="KochiMincho" style="normal" weight="normal"/>
            <font-triplet name="KochiMincho" style="normal" weight="bold"/>
            <font-triplet name="KochiMincho" style="italic" weight="normal"/>
            <font-triplet name="KochiMincho" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/batang.xml"
                kerning="yes" embed-url="/usr/share/fonts/ko/TrueType/batang.ttf">
            <font-triplet name="BaekmukBatang" style="normal" weight="normal"/>
            <font-triplet name="BaekmukBatang" style="normal" weight="bold"/>
            <font-triplet name="BaekmukBatang" style="italic" weight="normal"/>
            <font-triplet name="BaekmukBatang" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/lohit_ta.xml"
                kerning="yes" embed-url="/usr/share/fonts/ta/lohit_ta.ttf">
            <font-triplet name="LohitTamil" style="normal" weight="normal"/>
            <font-triplet name="LohitTamil" style="normal" weight="bold"/>
            <font-triplet name="LohitTamil" style="italic" weight="normal"/>
            <font-triplet name="LohitTamil" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/lohit_pa.xml"
                kerning="yes" embed-url="/usr/share/fonts/pa/lohit_pa.ttf">
            <font-triplet name="LohitPunjabi" style="normal" weight="normal"/>
            <font-triplet name="LohitPunjabi" style="normal" weight="bold"/>
            <font-triplet name="LohitPunjabi" style="italic" weight="normal"/>
            <font-triplet name="LohitPunjabi" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/lohit_hi.xml"
                kerning="yes" embed-url="/usr/share/fonts/hi/lohit_hi.ttf">
            <font-triplet name="LohitHindi" style="normal" weight="normal"/>
            <font-triplet name="LohitHindi" style="normal" weight="bold"/>
            <font-triplet name="LohitHindi" style="italic" weight="normal"/>
            <font-triplet name="LohitHindi" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/lohit_gu.xml"
                kerning="yes" embed-url="/usr/share/fonts/gu/lohit_gu.ttf">
            <font-triplet name="LohitGujarati" style="normal" weight="normal"/>
            <font-triplet name="LohitGujarati" style="normal" weight="bold"/>
            <font-triplet name="LohitGujarati" style="italic" weight="normal"/>
            <font-triplet name="LohitGujarati" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/lohit_bn.xml"
                kerning="yes" embed-url="/usr/share/fonts/bn/lohit_bn.ttf">
            <font-triplet name="LohitBengali" style="normal" weight="normal"/>
            <font-triplet name="LohitBengali" style="normal" weight="bold"/>
            <font-triplet name="LohitBengali" style="italic" weight="normal"/>
            <font-triplet name="LohitBengali" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/kochi-gothic-subst.xml"
                kerning="yes" embed-url="/usr/share/fonts/ja/TrueType/kochi-gothic-subst.ttf">
            <font-triplet name="KochiGothic" style="normal" weight="normal"/>
            <font-triplet name="KochiGothic" style="normal" weight="bold"/>
            <font-triplet name="KochiGothic" style="italic" weight="normal"/>
            <font-triplet name="KochiGothic" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/hline.xml"
                kerning="yes" embed-url="/usr/share/fonts/ko/TrueType/hline.ttf">
            <font-triplet name="BaekmukHeadline" style="normal" weight="normal"/>
            <font-triplet name="BaekmukHeadline" style="normal" weight="bold"/>
            <font-triplet name="BaekmukHeadline" style="italic" weight="normal"/>
            <font-triplet name="BaekmukHeadline" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/gulim.xml"
                kerning="yes" embed-url="/usr/share/fonts/ko/TrueType/gulim.ttf">
            <font-triplet name="BaekmukGulim" style="normal" weight="normal"/>
            <font-triplet name="BaekmukGulim" style="normal" weight="bold"/>
            <font-triplet name="BaekmukGulim" style="italic" weight="normal"/>
            <font-triplet name="BaekmukGulim" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/gkai00mp.xml"
                kerning="yes" embed-url="/usr/share/fonts/zh_CN/TrueType/gkai00mp.ttf">
            <font-triplet name="ARPLKaitiMGB" style="normal" weight="normal"/>
            <font-triplet name="ARPLKaitiMGB" style="normal" weight="bold"/>
            <font-triplet name="ARPLKaitiMGB" style="italic" weight="normal"/>
            <font-triplet name="ARPLKaitiMGB" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/dotum.xml"
                kerning="yes" embed-url="/usr/share/fonts/ko/TrueType/dotum.ttf">
            <font-triplet name="BaekmukDotum" style="normal" weight="normal"/>
            <font-triplet name="BaekmukDotum" style="normal" weight="bold"/>
            <font-triplet name="BaekmukDotum" style="italic" weight="normal"/>
            <font-triplet name="BaekmukDotum" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/bsmi00lp.xml"
                kerning="yes" embed-url="/usr/share/fonts/zh_TW/TrueType/bsmi00lp.ttf">
            <font-triplet name="ARPLMingti2LBig5" style="normal" weight="normal"/>
            <font-triplet name="ARPLMingti2LBig5" style="normal" weight="bold"/>
            <font-triplet name="ARPLMingti2LBig5" style="italic" weight="normal"/>
            <font-triplet name="ARPLMingti2LBig5" style="italic" weight="bold"/>

        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationMono-Regular.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationMono-Regular.ttf">
            <font-triplet name="LiberationMono" style="normal" weight="normal"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationMono-Italic.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationMono-Italic.ttf">
            <font-triplet name="LiberationMono" style="italic" weight="normal"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationMono-Bold.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationMono-Bold.ttf">
            <font-triplet name="LiberationMono" style="normal" weight="bold"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationMono-BoldItalic.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationMono-BoldItalic.ttf">
            <font-triplet name="LiberationMono" style="italic" weight="bold"/>
        </font>

        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSans-Regular.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSans-Regular.ttf">
            <font-triplet name="LiberationSans" style="normal" weight="normal"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSans-Italic.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSans-Italic.ttf">
            <font-triplet name="LiberationSans" style="italic" weight="normal"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSans-Bold.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSans-Bold.ttf">
            <font-triplet name="LiberationSans" style="normal" weight="bold"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSans-BoldItalic.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSans-BoldItalic.ttf">
            <font-triplet name="LiberationSans" style="italic" weight="bold"/>
        </font>

        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSerif-Regular.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSerif-Regular.ttf">
            <font-triplet name="LiberationSerif" style="normal" weight="normal"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSerif-Italic.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSerif-Italic.ttf">
            <font-triplet name="LiberationSerif" style="italic" weight="normal"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSerif-Bold.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSerif-Bold.ttf">
            <font-triplet name="LiberationSerif" style="normal" weight="bold"/>
        </font>
        <font metrics-url="/usr/share/publican/fop/font-metrics-0.93/LiberationSerif-BoldItalic.xml"
            kerning="yes" embed-url="/usr/share/fonts/liberation/LiberationSerif-BoldItalic.ttf">
            <font-triplet name="LiberationSerif" style="italic" weight="bold"/>
        </font-->
        <!--font metrics-url="/usr/share/publican/fop/font-metrics-0.93/"
            kerning="yes"
            embed-url="/usr/share/fonts/">
            <font-triplet name="" style="normal" weight="normal"/>
        </font-->
    </fonts>

      <!-- This option lets you specify additional options on an XML handler -->
      <!--xml-handler namespace="http://www.w3.org/2000/svg">
        <stroke-text>false</stroke-text>
      </xml-handler-->

    </renderer>

    <renderer mime="application/postscript">
      <!-- This option forces the PS renderer to rotate landscape pages -->
      <!--auto-rotate-landscape>true</auto-rotate-landscape-->
      
      <!-- This option lets you specify additional options on an XML handler -->
      <!--xml-handler namespace="http://www.w3.org/2000/svg">
        <stroke-text>false</stroke-text>
      </xml-handler-->
    </renderer>

    <renderer mime="application/vnd.hp-PCL">
    </renderer>

    <!-- MIF does not have a renderer
    <renderer mime="application/vnd.mif">
    </renderer>
    -->

    <renderer mime="image/svg+xml">
      <format type="paginated"/>
      <link value="true"/>
      <strokeText value="false"/>
    </renderer>

    <renderer mime="application/awt">
    </renderer>

    <renderer mime="image/png">
      <!--transparent-page-background>true</transparent-page-background-->
    </renderer>

    <renderer mime="image/tiff">
      <!--transparent-page-background>true</transparent-page-background-->
      <!--compression>CCITT T.6</compression-->
    </renderer>

    <renderer mime="text/xml">
    </renderer>

    <!-- RTF does not have a renderer
    <renderer mime="text/rtf">
    </renderer>
    -->

    <renderer mime="text/plain">
      <pageSize columns="80"/>
    </renderer>

  </renderers>

</fop>

