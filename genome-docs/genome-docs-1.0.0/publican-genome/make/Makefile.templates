#Makefile for Red Hat Documentation
#Created by Jeff Fearn <jfearn@redhat.com>
#    Copyright (C) 2008 Red Hat, Inc.
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#

# Default values, only set if not set in book Makefile
# default 2 section levels in TOC
TOC_SECTION_DEPTH	?= 2
CHUNK_SECTION_DEPTH	?= 4
PO2ENTITY		?= po2entity
ENITY2PO		?= entity2po
XMLPOMERGE		?= poxmerge
POTMERGE		?= potmerge
CHUNK_FIRST		?= 1
SHOW_REMARKS	?= 0
EMBEDTOC	?= 0
BRAND		?= common
FOP_VER		= $(shell fop -v 2>/dev/null | grep FOP | sed -e 's/^[^0..9]*//g';)
DOC_URL		?= http://www.redhat.com/doc

#CATALOGS_OVERRIDE

###############################################################################
# SECTION: PUBLISHING 
###############################################################################

###############################################################################
# PUBLISH_template
# produces phony rules like:
#		publish-html-ko-KR: html-ko-KR
#		publish-pdf-ja-JP: pdf-ja-JP
# Handles having different public/local names for i18n
# Params:
#	1: Format (pdf/html etc)
#	2: Language (ko-KR, bn-IN etc)
###############################################################################

define	PUBLISH_template
.PHONY:	publish-${1}-${2}
publish-${1}-${2}:: ${1}-${2}
	@echo "START: publish-${1}-${2} `date`"
	@mkdir -p publish/${2}/$(PRODUCT)/$(VERSION)/${1}/$(DOCNAME)
	@cp -rf tmp/${2}/${1}/* publish/${2}/$(PRODUCT)/$(VERSION)/${1}/$(DOCNAME)/. 
	@echo "END: publish-${1}-${2} `date`"
endef

###############################################################################
# SECTION: BUILDING 
###############################################################################

###############################################################################
#
# Template to create full xml for each language.
# Parameters:
#	1: Language (ko-KR, bn-IN etc)
#	2: Doc Name
#
###############################################################################
define	XML_Template
.PHONY:	xml-${1}
xml-${1}:: pre
	@echo "START: xml-${1} `date`"
	-@mkdir -p tmp/${1}/xml_tmp
	-@mkdir -p tmp/${1}/xml/Common_Content
ifdef EXTRA_DIRS
ifneq "$(EXTRA_DIRS)" ""
	-@cd tmp/${1}/xml && mkdir -p $(EXTRA_DIRS)
	-@cd tmp/${1}/xml_tmp && mkdir -p $(EXTRA_DIRS)
endif
endif
ifeq "${1}" "$(findstring ${1},$(IGNORED_TRANSLATIONS))"
	@echo "	Bypassing translation for ${1}"
	@cp -rf tmp/$(XML_LANG)/* tmp/${1}/.
else
ifeq "${1}" "$(XML_LANG)"
	@cp -rf ${1}/* tmp/${1}/xml_tmp
else
	@if [ ! -d ${1} ]; then \
		echo "Invalid Build request, language directory ${1} does not exist."; \
		exit 0; \
	fi
	@if [ `find ${1} -name "*.po" | wc -l` -le "0" ]; then \
		echo "Invalid Build request, no PO files exist for language ${1}."; \
		exit 0; \
	fi
#	@cd $(XML_LANG) && for xml_file in `find . -name "*.xml" | sort`; do \
		po_file=`echo $$$$xml_file | sed -e 's/\.xml/\.po/'`;\
		/usr/bin/xml2po -e -l ${1} -p ../${1}/$$$$po_file -o ../tmp/${1}/xml_tmp/$$$$xml_file $$$$xml_file;\
	done;
	@$(XMLPOMERGE) --langs=${1} --output=tmp/${1}/xml_tmp --sub-dirs="$(EXTRA_DIRS_C)" --lang-dir=${1} $(XML_LANG)/${2}.xml
endif
# copy common files
	@if [ -d $(COMMON_CONTENT)/$(BRAND)/$(XML_LANG) ]; then \
		echo "	copying $(BRAND)/$(XML_LANG) Common_Content"; \
		cp -rf $(COMMON_CONTENT)/$(BRAND)/$(XML_LANG)/* tmp/${1}/xml/Common_Content/.; \
	fi
	@if [ -d $(COMMON_CONTENT)/$(BRAND)/${1} ]; then \
		echo "	copying $(BRAND)/${1} Common_Content"; \
		cp -rf $(COMMON_CONTENT)/$(BRAND)/${1}/* tmp/${1}/xml/Common_Content/.; \
	fi
# copy original local ent, then copy translated local ent over the top
	@if [ -f $(XML_LANG)/${2}.ent ]; then \
		cp -rf $(XML_LANG)/${2}.ent tmp/${1}/xml/.; \
		sed -i -e 's/docbookx.dtd" \[/docbookx.dtd" \[\n<!ENTITY % BOOK_ENTITIES SYSTEM "..\/${2}.ent">\n%BOOK_ENTITIES\;/' tmp/${1}/xml/Common_Content/*.xml; \
	fi
	@if [ -f ${1}/${2}.ent ]; then cp -rf ${1}/${2}.ent tmp/${1}/xml/.; fi
# copy original images, then copy translated images over the top
	@if [ -d $(XML_LANG)/images ]; then cp -rf $(XML_LANG)/images tmp/${1}/xml/.; fi
	@if [ -d ${1}/images ]; then cp -rf ${1}/images tmp/${1}/xml/.; fi
# copy extras
	@if [ -d $(XML_LANG)/extras ]; then cp -rf $(XML_LANG)/extras tmp/${1}/xml/.; fi
	@if [ 1 ]; then \
		if [ "$(ARCH)" != "" ]; then \
			args=`echo "-arch $(ARCH)"`; \
		fi; \
		if [ "$(CONDITION)" != "" ]; then \
			args=`echo "$$$$args -condition $(CONDITION)"`; \
		fi; \
		if [ "$(DTD_VER)" != "" ]; then \
			args=`echo "$$$$args -dtd $(DTD_VER)"`; \
		fi; \
		pushd tmp/${1}/xml_tmp > /dev/null; \
			echo "cleaning files"; \
			for file in `find . -name "*.xml" | sort`; do \
				file=`echo $$$$file | sed -e 's/^\.\///'`; \
				echo "	$$$$file"; \
				$(XMLCLEAN) -in $$$$file -out ../xml/$$$$file -book ${2} -lang ${1} $$$$args; \
			done; \
		popd > /dev/null; \
	fi;
	@find tmp -name CVS | xargs rm -rf
	@find tmp -name '.svn' | xargs rm -rf
	@cd tmp/${1}/xml && rmImages
endif
	@echo "END: xml-${1} `date`"
endef

###############################################################################
#
# Template to allow translations to be HTML/PDF-afied
# produces phony rules like: html-ko-KR, pdf-ja-JP etc.
# Parameters:
#	1: Format (pdf/html etc)
#	2: Language (ko-KR, bn-IN etc)
#	3: XSL file
#	4: Extra Deps
###############################################################################
define	BUILD_Template
.PHONY:	${1}-${2}
${1}-${2}:: xml-${2} test-${2} ${4}
	@echo "START: ${1}-${2} `date`"
	@mkdir -p tmp/${2}/${1}
ifeq "${2}" "$(findstring ${2},$(IGNORED_TRANSLATIONS))"
	@echo "	Bypassing translation for ${2}"
	@cp -rf tmp/$(XML_LANG)/${1} tmp/${2}/.
else
ifeq "${1}" "txt"
	@cd tmp/${2} && /usr/bin/links -dump -dump-width 72 -force-html -dump-charset UTF8 -no-numbering html-single/index.html > txt/$(DOCNAME).txt
else
ifeq "${1}" "pdf"
	@cd tmp/${2}/xml; \
		if [ -f $(COMMON_CONFIG)/xsl/$(BRAND)/${3} ]; then \
			xsl_file="$(COMMON_CONFIG)/xsl/$(BRAND)/${3}"; \
		else \
			xsl_file="$(COMMON_CONFIG)/xsl/${3}"; \
		fi; \
		if [ "$(FOP_VER)" = "0.20.5" ]; then \
			$(CATALOGS) xsltproc --nonet --stringparam toc.section.depth "$(TOC_SECTION_DEPTH)" --stringparam chunk.section.depth "$(CHUNK_SECTION_DEPTH)" --stringparam confidential ${CONFIDENTIAL} --stringparam l10n.gentext.language "${2}" --stringparam show.comments $(SHOW_REMARKS) --xinclude -o $(DOCNAME).fo $$$$xsl_file $(DOCNAME).xml; \
		else \
			$(CATALOGS) xsltproc --nonet --stringparam toc.section.depth "$(TOC_SECTION_DEPTH)" --stringparam chunk.section.depth "$(CHUNK_SECTION_DEPTH)" --stringparam confidential ${CONFIDENTIAL} --stringparam l10n.gentext.language "${2}" --stringparam show.comments $(SHOW_REMARKS) --stringparam fop.extensions "0" --stringparam fop1.extensions "1" --stringparam admon.graphics.extension ".png" --xinclude -o $(DOCNAME).fo $$$$xsl_file $(DOCNAME).xml ;\
		fi
	@cd tmp/${2}/xml; \
		if [ ! -f $(COMMON_CONFIG)/fop/fop-$(FOP_VER).xconf ]; then \
			echo "ERROR: config file for FOP version '$(FOP_VER)' not found"; \
			exit 1; \
		fi; \
		fop -q -c $(COMMON_CONFIG)/fop/fop-$(FOP_VER).xconf -fo $(DOCNAME).fo -pdf $(DOCNAME).pdf;
	@mv -f tmp/${2}/xml/$(DOCNAME).${1} tmp/${2}/${1}/.
else
ifeq "${1}" "html-single"
	@cd tmp/${2}/${1}; \
		if [ -f $(COMMON_CONFIG)/xsl/$(BRAND)/${3} ]; then xsl_file="$(COMMON_CONFIG)/xsl/$(BRAND)/${3}"; else xsl_file="$(COMMON_CONFIG)/xsl/${3}";fi; \
		$(CATALOGS) xsltproc --nonet --stringparam toc.section.depth "$(TOC_SECTION_DEPTH)" --stringparam chunk.section.depth "$(CHUNK_SECTION_DEPTH)" --stringparam confidential "${CONFIDENTIAL}" --stringparam profile.lang "${2}" --stringparam l10n.gentext.language "${2}" --stringparam embedtoc $(EMBEDTOC) --stringparam show.comments $(SHOW_REMARKS) --xinclude  -o index.html $$$$xsl_file ../xml/$(DOCNAME).xml
# Copy original images to output dir, then copy any translated images over the top
	-@cp -rf tmp/${2}/xml/images  tmp/${2}/${1}/.
	-@cp -rf tmp/${2}/xml/Common_Content  tmp/${2}/${1}/.
else
	@cd tmp/${2}/${1}; \
		if [ -f $(COMMON_CONFIG)/xsl/$(BRAND)/${3} ]; then xsl_file="$(COMMON_CONFIG)/xsl/$(BRAND)/${3}"; else xsl_file="$(COMMON_CONFIG)/xsl/${3}";fi; \
		$(CATALOGS) xsltproc --nonet --stringparam doc.url "$(DOC_URL)" --stringparam toc.section.depth "$(TOC_SECTION_DEPTH)" --stringparam chunk.section.depth "$(CHUNK_SECTION_DEPTH)" --stringparam confidential "${CONFIDENTIAL}" --stringparam profile.lang "${2}" --stringparam l10n.gentext.language "${2}" --stringparam show.comments $(SHOW_REMARKS) --stringparam embedtoc $(EMBEDTOC) --stringparam chunk.first.sections "$(CHUNK_FIRST)" --xinclude $$$$xsl_file ../xml/$(DOCNAME).xml
# Copy original images to output dir, then copy any translated images over the top
	-@cp -rf tmp/${2}/xml/images  tmp/${2}/${1}/.
	-@cp -rf tmp/${2}/xml/Common_Content  tmp/${2}/${1}/.
endif
endif
endif
	@find tmp -name CVS | xargs rm -rf
	@find tmp -name '.svn' | xargs rm -rf
endif
	@echo "END: ${1}-${2} `date`"
endef


###############################################################################
# SECTION: PO FILES 
###############################################################################

###############################################################################
# Template to update PO files
# produces phony rules like: update-po-ko-KR, update-po-ja-JP etc.
# Parameters:
#	1: Language (ko-KR, bn-IN etc)
#	2: Doc Name
###############################################################################
define	PO_Update_Template
.PHONY:	update-po-${1}
update-po-${1}:: update-pot
	@echo "START: update-po-${1} `date`"
	@if [ ! -d ${1} ]; then mkdir -p ${1}; fi
ifneq "$(EXTRA_DIRS)" ""
	@cd ${1} && mkdir -p $(EXTRA_DIRS)
endif
	@cd pot && for pot_file in `find . -name "*.pot" | sed -e 's/^\.\///' | sort`; do \
		po_file=`echo $$$$pot_file | sed -e 's/\.pot/\.po/'`; \
		if [ ! -f ../${1}/$$$$po_file ]; then \
			echo "Creating new po file: ../${1}/$$$$po_file"; \
			msginit -l ${1} --no-translator -i $$$$pot_file -o ../${1}/$$$$po_file; \
		else \
			echo "Merging existing po file: ../${1}/$$$$po_file"; \
			echo -n "	Before:	";\
			msgfmt -c -f --statistics ../${1}/$$$$po_file;\
			msgmerge --quiet --backup=none --update ../${1}/$$$$po_file $$$$pot_file; \
		fi; \
		echo -n "	After:	" ;\
		msgfmt -c -f --statistics ../${1}/$$$$po_file;\
		xml_file=`echo $$$$pot_file | sed -e 's/\.pot/\.xml/'`; \
		if [ ! -f ../$(XML_LANG)/$$$$xml_file ]; then \
			echo "WARNING: No source xml file exists for $$$$pot_file"; \
		fi; \
	done;
#ifneq "$(EXTRA_DIRS_C)" ""
#	@$(POTMERGE) --lang=${1} --lang-dir=${1} --sub-dirs="$(EXTRA_DIRS_C)" pot/${2}.pot
#else
#	@$(POTMERGE) --lang=${1} --lang-dir=${1} pot/${2}.pot
#endif
	@echo "END: update-po-${1} `date`"
endef

###############################################################################
# Template for Translation Totals Reports: Prints to command line
#
# produces phony rules like: report-total-ko-KR, report-total-ja-JP etc.
# Parameters:
#	1: Language (ko-KR, bn-IN etc)
###############################################################################

define	PO_Report_Totals_Template
.PHONY:	report-total-${1}
report-total-${1}::
ifneq "${1}" "$(XML_LANG)"
	@echo "START: report-total-${1} `date`"
	@if [ ! -d ${1} ]; then \
		echo "Invalid Report request, language directory ${1} does not exist."; \
		exit 0; \
	fi
	@if [ `find ${1} -name "*.po" | wc -l` -le "0" ]; then \
		echo "Invalid Report request, no PO files exist for language ${1}."; \
		exit 0; \
	fi
	@po2sgml --total --report ${1}/*.po
	@echo "END: report-total-${1} `date`"
endif
endef

###############################################################################
# Template for Translation Reports: redirects to file
#
# produces phony rules like: report-ko-KR, report-ja-JP etc.
# Parameters:
#	1: Language (ko-KR, bn-IN etc)
###############################################################################

define	PO_Report_Template
.PHONY:	report-${1}
report-${1}::
ifneq "${1}" "$(XML_LANG)"
	@echo "START: report-${1} `date`"
	@if [ ! -d ${1} ]; then \
		echo "Invalid Report request, language directory ${1} does not exist."; \
		exit 0; \
	fi
	@if [ `find ${1} -name "*.po" | wc -l` -le "0" ]; then \
		echo "Invalid Report request, no PO files exist for language ${1}."; \
		exit 0; \
	fi
	@mkdir -p tmp/reports
	@cd ${1} && po2sgml --report *.po >& ../tmp/reports/${1}.txt
	@echo "END: report-${1} `date`"
endif
endef

###############################################################################
# Template for Testing PO files
#
# produces phony rules like: test-po-ko-KR.
# Parameters:
#	1: Language (ko-KR, bn-IN etc)
###############################################################################

define	TEST_PO_Template
.PHONY:	test-po-${1}
test-po-${1}::
	@echo "START: $@ `date`"
	@for file in find ${1} -name "*.po"; do \
		echo "	checking $$$$file"; \
		po-validateXML $$$$file;\
	done
	@echo "END: $@ `date`"
endef


###############################################################################
# Template for Testing
#
# produces phony rules like: test-ko-KR:: xml-ko-KR.
# Parameters:
#	1: Language (ko-KR, bn-IN etc)
###############################################################################

define	TEST_Template
.PHONY:	test-${1}
test-${1}:: xml-${1}
	@echo "START: test-${1} `date`"
	@$(CATALOGS) xmllint --nonet --noout --postvalid --xinclude tmp/${1}/xml/$(DOCNAME).xml
	@echo "END: test-${1} `date`"
endef


###############################################################################
# SPEC_Template
#
# Parameters:
#	1: Format
###############################################################################

define	SPEC_Template
.PHONY:	spec-${1}

spec-${1}::
	@echo "START: spec-${1} `date`"
	@mkdir -p tmp/rpm/SPECS
	@for lang in $(TRANSLATIONS); do \
		$(CATALOGS) $(XSLTPROC) --nonet --path "$(PWD)" --stringparam book-title "$(PRODUCT)-$(DOCNAME)-$(VERSION)-${1}-$$$$lang" \
		--stringparam lang "$$$$lang" \
		--stringparam  prod "$(PRODUCT)" \
		--stringparam  ver "$(VERSION)" \
		--stringparam  format "${1}" \
		--stringparam  docname "$(DOCNAME)" \
		-o tmp/rpm/SPECS/$(PRODUCT)-$(DOCNAME)-$(VERSION)-${1}-$$$$lang.spec \
		$(COMMON_CONFIG)/xsl/format-specific-spec.xsl tmp/$(XML_LANG)/xml/$(DOC_TYPE)_Info.xml;\
	done
	@echo "END: spec-${1} `date`"
endef


###############################################################################
# Template for per format Tar
#
# produces phony rules like: tar-html:: publish-html-all.
# Parameters:
#	1: Format (html, pdf, etc)
###############################################################################

define	TAR_Template
.PHONY:	tar-${1}
tar-${1}:: publish-${1}-all
	@echo "START: tar-${1} `date`"
	@for lang in $(TRANSLATIONS); do \
		pushd publish > /dev/null; \
		mkdir -p $$$$lang/$(PRODUCT)/$(VERSION)/tar; \
		tar -czf $$$$lang/$(PRODUCT)/$(VERSION)/tar/$(PRODUCT)-$(DOCNAME)-$(VERSION)-${1}-$$$$lang-$(RELEASE)-0.tgz $$$$lang/$(PRODUCT)/$(VERSION)/${1}/$(DOCNAME); \
		popd > /dev/null; \
	done;
	@echo "END: tar-${1} `date`"
endef



###############################################################################
# Template for per format SRPMS
#
# produces phony rules like: srpm-html:: tar-html spec-html.
# Parameters:
#	1: Format (html, pdf, etc)
###############################################################################
define	SRPM_Template
.PHONY:	srpm-${1}
srpm-${1}:: EMBEDTOC = 1
srpm-${1}:: publish-${1}-all tar-${1} spec-${1}
	@echo "START: srpm-${1} `date`"
	@mkdir -p tmp/rpm/SOURCES tmp/rpm/SRPMS publish/SRPMS
	@for lang in $(TRANSLATIONS); do \
		cp publish/$$$$lang/$(PRODUCT)/$(VERSION)/tar/$(PRODUCT)-$(DOCNAME)-$(VERSION)-${1}-$$$$lang-$(RELEASE)-0.tgz tmp/rpm/SOURCES/.; \
		rpmbuild --define "_topdir $(PWD)/tmp/rpm" -bs tmp/rpm/SPECS/$(PRODUCT)-$(DOCNAME)-$(VERSION)-${1}-$$$$lang.spec; \
		mv tmp/rpm/SRPMS/$(PRODUCT)-$(DOCNAME)-$(VERSION)-${1}-$$$$lang-$(RELEASE)-0.src.rpm publish/SRPMS/.; \
	done;
	@echo "END: srpm-${1} `date`"
endef

###############################################################################
###############################################################################

clean::
	@echo "START: $@ `date`"
	@rm -rf tmp publish
	@echo "END: $@ `date`"

# Clean all the books in this set
clean_set_books::
	@echo "START: $@ `date`"
	@for dir in $(BOOKS); do \
		if [ -d "$$dir" ] ; then \
			pushd $$dir > /dev/null; \
			make clean; \
			popd > /dev/null; \
		else \
			echo "CAN'T CHANGE DIRS TO $$dir FOR CLEANING" ; \
		fi; \
	done;
	@echo "END: $@ `date`"

# build all the books in this set
set_books::
	@echo "START: $@ `date`"
	@for dir in $(BOOKS); do \
		if [ -d "$$dir" ] ; then \
			pushd $$dir > /dev/null; \
			make clean; \
			make clean_ids; \
			make xml-all; \
			popd > /dev/null; \
		else \
			echo "$$dir DIRECTORY NOT FOUND"; \
		fi; \
		for lang in $(TRANSLATIONS); do mkdir -p tmp/$$lang/xml; \
			cp -rf $$dir/tmp/$$lang/xml tmp/$$lang/xml/$$dir; \
			if [ -d "tmp/$$lang/xml/$$dir/images" ]; then \
				mkdir -p tmp/$$lang/xml/images; \
				mv -f tmp/$$lang/xml/$$dir/images/* tmp/$$lang/xml/images/.; \
				rm -rf tmp/$$lang/xml/$$dir/images; \
			fi; \
		done; \
	done;
	@echo "END: $@ `date`"

# remove po and pot files if the xml files has been removed
#
# WARNING: Make sure you run `cvs up -d` first!!!
#
rm_rs_po:
	@echo "START: $@ `date`"
	@for lang in $(OTHER_LANGS) pot ; do \
		cd $$lang && \
		for file in `find . -name "*.po*" | sed -e 's/^\.\///' | sort` ; do \
			xmlfile=`echo $$file | sed -e 's/po[t]*$$/xml/'`; \
			if [ ! -f ../$(XML_LANG)/$$xmlfile ]; then \
				echo "removing: $$lang/$$file"; \
				cvs remove -f $$file; \
			fi; \
		done; \
		cd ..; \
	done;
	@echo "END: $@ `date`"

# Modify the id's of tags.
# WARNING: this changes the source xml, it does not update translations
clean_ids:
	@echo "START: $@ `date`"
	@cd $(XML_LANG) && \
	for file in `find . -name "*.xml" | sed -e 's/^\.\///' | sort`; do \
		echo "	$$file"; \
		$(XMLCLEAN) -in $$file -out $$file -book $(DOCNAME) -cleanids ; \
	done && cd ..;
	@echo "END: $@ `date`"

