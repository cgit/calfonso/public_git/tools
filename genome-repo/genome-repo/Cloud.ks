#platform=x86, AMD64, or Intel EM64T
# System authorization information
auth  --useshadow  --enablemd5
# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all --initlabel
# Use text mode install
text
# Firewall configuration
firewall --disabled
# Run the Setup Agent on first boot
firstboot --disable
# System keyboard
keyboard us
# System language
lang en_US
# Use network installation
url --url=$tree
key --skip

# If any cobbler repo definitions were referenced in the kickstart profile, include them here.
$yum_repo_stanza

# Network information
network --bootproto=dhcp --device=eth0 --onboot=on --noipv6
# Reboot after installation
reboot

#Root password
rootpw --iscrypted $1$z8ATNbdx$URqSRM3RkCgBCTIKD4Z3W.
# SELinux configuration
selinux --disabled
# Do not configure the X Window System
skipx
# System timezone
timezone  America/New_York
# Install OS instead of upgrade
install
# Clear the Master Boot Record
zerombr

# Setup LVM for the Cloud machines
clearpart --all --initlabel
partition /boot --fstype=ext3 --size=200
partition pv.01 --size=10240
partition pv.02 --size=10240 --grow
volgroup HostVolGroup00 pv.01
volgroup HostVolGroupImages pv.02
logvol swap --fstype swap --name=Swap --vgname=HostVolGroup00 --size=2048
logvol / --fstype ext3 --name=Root --vgname=HostVolGroup00 --size=10240 --grow
logvol /images --fstype ext3 --name=Images --vgname=HostVolGroupImages --size=10240 --grow

%pre
$kickstart_start

%packages
kernel-PAE
kvm
bridge-utils
libvirt
koan
git
httpd-cloudmasterd
httpd-cloudmasterd-redirect
rubygem-cloudmasterd
genome-bridge

%post
# Extend the number of loopback devices allowed
echo "options loop max_loop=255" >> /etc/modprobe.conf

# Set the certmaster in the minion configuration
sed -i "s/certmaster = certmaster/certmaster = $certmaster/g" /etc/certmaster/minion.conf

# Turn on autosigning for func masters by default
if [ "$certmaster" == "localhost" ]; then
  sed -i "s/autosign = no/autosign = yes/g" /etc/certmaster/certmaster.conf
fi

# Chkconfig the cloudmasterd service for the cloud masters
if [ "$certmaster" == "localhost" ]; then
  /sbin/chkconfig --add cloudmasterd
  /sbin/chkconfig cloudmasterd on
  /sbin/chkconfig --add httpd
  /sbin/chkconfig httpd on
fi

# Need for the bridge
/sbin/chkconfig NetworkManager off
/sbin/chkconfig network on
/sbin/chkconfig genome-bridge on

$yum_config_stanza
$kickstart_done
