#platform=x86, AMD64, or Intel EM64T
# System authorization information
auth  --useshadow  --enablemd5
# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all --initlabel
# Use text mode install
text
# Firewall configuration
firewall --disabled
# Run the Setup Agent on first boot
firstboot --disable
# System keyboard
keyboard us
# System language
lang en_US
# Use network installation
url --url=$tree
key --skip

# If any cobbler repo definitions were referenced in the kickstart profile, include them here.
$yum_repo_stanza

# Network information
network --bootproto=dhcp --device=eth0 --onboot=on --noipv6
# Reboot after installation
reboot

#Root password
rootpw --iscrypted $1$z8ATNbdx$URqSRM3RkCgBCTIKD4Z3W.
# SELinux configuration
selinux --disabled
# Do not configure the X Window System
skipx
# System timezone
timezone  America/New_York
# Install OS instead of upgrade
install
# Clear the Master Boot Record
zerombr

# Magically figure out how to partition this thing
%include /tmp/partinfo

%pre
# Auto partitioning
SNIPPET::partition_select
$kickstart_start

%packages
git
puppet
genome-firstboot

%post
FIRSTBOOT_CFG=/etc/sysconfig/genome-firstboot
echo RUN_BOOTSTRAP=YES > $FIRSTBOOT_CFG 
echo export GENOME_REPO=$genome_repo >> $FIRSTBOOT_CFG
echo export FQDN=$fqdn >> $FIRSTBOOT_CFG

$yum_config_stanza
$kickstart_done
