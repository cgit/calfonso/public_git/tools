filebucket { main: server => puppet }

Exec {
    path        => '/sbin:/usr/sbin:/usr/local/sbin:/bin:/usr/bin:/usr/local/bin'
}
    

File {
    ignore      => [ '.svn' ],
    owner       => root,
    group       => root,
}
