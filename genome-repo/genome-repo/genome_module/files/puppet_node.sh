#!/bin/sh

# Repo machines keep their yaml in a "special" location.  This is to keep
# people from being able to change repo configuration from genomed
if [ `hostname` == $1 ]; then
    cat /etc/genome/repo.yaml
else
    cat /etc/genome/nodes/$1/node.yaml
fi
