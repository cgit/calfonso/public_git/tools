# Copyright (C) 2008 Red Hat, Inc

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# a long with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

################################################################################
#Define new facts here. You can later map them to machine types in the         #
#following section.                                                            #
#                                                                              #
# One thing to note is the special syntax allowed when setting default values  #
# for facts.  In the default string you are allowed to use "%repo%" or         #
# "%machine_name%" which will get substituted appropriately whenever this data #
# is used (eg, genome-bootstrap uses this for setting context aware default   #
# values for facts).                                                           #
#                                                                              #
# If a fact needs to be on all machines types set ":on_all_nodes"              #
# appropriately.  Otherwise in the machine declaration section you need to     #
# specify what facts will be used for a particular machine.                    #
################################################################################

# newfact("puppetserver", :on_all_nodes => true) do
#   set_desc "The puppetmaster used for all configuration.  If you are " + 
#            "provisioning a Repo machine, this should be it's FQDN"
#   set_default "%repo%-repo.usersys.redhat.com"
# end

# newfact("logserver") do
#   set_desc "The machine to send syslog output to"
# end

##############################################################################
# Machines definitions go here. It is important that this happens after the  #
# fact declarations.  Aside from that, order does not matter.                #
##############################################################################

# classes_on_all_machine_types "puppet::client"

# newmachine("jboss-dev") do
#   include_facts "logserver"
#   set_classes "jboss::dev", "java", "jboss::server::web",
#               "mysql::standalone", "jboss::ds::messaging", "jboss::ds::esb",
#               "apache::ssl", "apache::ajp_http","apache::ajp_devel", 
# end
