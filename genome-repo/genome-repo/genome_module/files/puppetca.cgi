#!/bin/env ruby
require 'cgi'
require 'erb'

cgi = CGI.new
cgi.out("text/plain") do
  hostname = cgi['clean']
  # Since ` only returns stdout we redirect stderr
  `sudo /usr/sbin/puppetca --clean #{hostname} 2>&1` 
end
