class genomerepo {
    class base {
        $user = 'genome' 

        group { "$user":
            ensure      => present,
            require     => User[$user],
        }

        user { "$user":
            ensure      => present,
            membership  => minimum,
            shell       => "/bin/bash",
            home        => "/var/lib/$user",
            managehome  => true,
        }

	# This is a little heavy-handed.  Basically it's a temporary solution
	# until we decide whether or not the RPM should setup ownership on this
	# directory.  
        # 
        # If this turns out to hurt performance we could also use a cronjob
        exec { "Verify permissions are correct":
            command     => "/bin/chown -R ${user}:${user} /pub",
            require     => File["/pub"],
        }

        file { "/pub":
            ensure      => directory,
            owner       => $user,
            group       => $user,
        }

        file { "/pub/git":
            ensure      => directory,
            owner       => $user,
            group       => $user,
        }

        file { "/pub/git/puppet":
            ensure      => directory,
            owner       => $user,
            group       => $user,
        }
    }

    class xinetd inherits base {
        package { "xinetd":
            ensure      => installed,
        }

        service { "xinetd":
            ensure      => running,
            hasrestart  => true,
            hasstatus   => true,
            require     => Package["xinetd"],
        }
    }
 
    # This definition requires that you have included the xinetd class
    # http://groups.google.com/group/puppet-users/browse_thread/thread/3141cc38da909fe1 
    define xinetd::dropfile($module) {
        file { "/etc/xinetd.d/${name}":
            owner       => "root",
            group       => "root",
            ensure      => present,
            mode        => 0644,
            source      => "puppet:///${module}/${name}",
            notify      => Service["xinetd"],
        }
    }

    class httpd inherits base {
        package { "httpd":
            ensure     => installed
        }

        service { "httpd":
            ensure      => running,
            hasstatus   => true,
            # There is a bug in the httpd service script
            restart     => "/bin/sleep 5 && /sbin/service httpd graceful",
            require     => Package["httpd"],
        } 
    }
 
    class gitrepo inherits base {
        include httpd
        include xinetd

        package { "gitweb":
            ensure      => installed,
            notify      => Service["httpd"],
        }

        package { "git-daemon":
            ensure      => installed,
        }

        genomerepo::xinetd::dropfile { "git-daemon": 
            module      => "genomerepo",
            require     => Package["git-daemon"],
        }
    }

    class puppet inherits base {
        $puppetdir = "/etc/puppet"

	# Don't just change this value willy-nilly.  The git hooks rely on
	# this.
        $third_party_module_dir = "/etc/puppet/modules/main"

        file { "$third_party_module_dir":
            ensure      => directory,
            mode        => 0755,
            owner       => $user,
            group       => $user,
        }

        file { "${puppetdir}/puppet.conf":
            mode        => 0644,
            content     => template("genomerepo/puppet.conf.appliance.erb"),
            require     => Package["puppet"],
        }

        package { ["puppet", "puppet-server"]:
            ensure      => installed,
        }

        service { "puppet":
            ensure      => running,
            enable      => true,
            hasrestart  => true,
            hasstatus   => true,
            require     => File["${puppetdir}/puppet.conf"]
        }

        service { "puppetmaster":
            ensure      => running,
            enable      => true,
            hasrestart  => true,
            hasstatus   => true,
            require     => [Package["puppet-server"], File["${puppetdir}/puppet.conf"]],
	    before      => Service["puppet"],
        }

        file { "external-nodes":
            name        => '/usr/local/bin/puppet_node.sh',
            mode        => 0755,
            owner       => puppet,
            group       => puppet,
            source      => "puppet:///genomerepo/puppet_node.sh",
            notify      => Service["puppetmaster"],
            before      => File["${puppetdir}/puppet.conf"],
        }

    	puppet::file { "fileserver.conf": }
        puppet::file { "autosign.conf": }
        puppet::file { "manifests/site.pp": }

    	file { ["${puppetdir}/modules", "${puppetdir}/plugins"]: 
            ensure      => directory,
            mode        => 0755,
            before      => Service["puppetmaster"],
	}

        define puppet::file {
            file {"${puppetdir}/${name}":
                source  => "puppet:///genomerepo/${name}",
                before  => [Service["puppetmaster"], File["${puppetdir}/puppet.conf"]],
                require => Package["puppet-server"],
	    }
	}
    }

    class cobbler inherits base { 
        group { "apache":
            ensure     => present,
        }

        user { "apache":
            ensure     => present,
            require    => Group["apache"],
        }

        package { "cobbler":
            ensure   => installed,
            notify   => Service["httpd"],
        }

        package { ["yum-utils", "syslinux"]:
	        ensure   => installed, 
        }

        service { "cobblerd":
            ensure      => running,
            hasrestart  => true,
            hasstatus   => true,
            require     => Package["cobbler"],
        }
    
        file { "/etc/cobbler/users.digest":
             owner       => "root",
             group       => "root",
             ensure      => present,
             mode        => 0660,
             source      => "puppet:///genomerepo/users.digest",
             require     => Package["cobbler"],
        }

        file { "/etc/cobbler/settings": 
             owner       => "root",
             group       => "root",
             ensure      => present,
             mode        => 0664,
             content     => template("genomerepo/settings.erb"),
             require     => Package["cobbler"],
             notify      => Service["cobblerd"],
        }

        file { "/etc/cobbler/modules.conf": 
             owner       => "root",
             group       => "root",
             ensure      => present,
             mode        => 0664,
             source      => "puppet:///genomerepo/modules.conf",
             require     => Package["cobbler"],
             notify      => Service["cobblerd"],
        }

        cron { "reposync":
             command     => "/usr/bin/cobbler reposync",
             user        => root,
             hour        => 2,
             minute      => 0
        }

        if $cobbler_master {
            $ks_mirror = "/var/www/cobbler/ks_mirror"
            file { $ks_mirror:
                 ensure      => directory,
                 owner       => "apache",
                 group       => "apache",
                 require     => User["apache"],
            }

            mount { $ks_mirror:
                 ensure      => mounted,
                 device      => "${cobbler_master}:${ks_mirror}",
                 fstype      => "nfs",
                 options     => "ro,nosuid,nodev,noatime,intr,hard,tcp",
                 require     => [Package["cobbler"], File["$ks_mirror"]],
                 notify      => Exec["cobbler-replicate"],
            }

            exec { "cobbler-replicate":
                command     => "/usr/bin/cobbler replicate --master=${cobbler_master}",
                refreshonly => true,
            }
        }
    }

    # We need a least some way for custom machine types to configured puppetd
    # out of the box.  This class is available to any custom machine types.
    # Note: This class is NOT compatible with the appliance class.
    class client {
        $puppetdir = "/etc/puppet"

        file { "${puppetdir}/puppet.conf":
            mode        => 0644,
            content     => template("genomerepo/puppet.conf.client.erb"),
            require     => Package["puppet"],
        }

        package { "puppet":
            ensure      => installed,
        }

        service { "puppet":
            ensure      => running,
            enable      => true,
            hasrestart  => true,
            hasstatus   => true,
            require     => File["${puppetdir}/puppet.conf"]
        }

    }
    
    # This class needs to have some sort of auto backup support
    # since it will be more 'mission critical'
    class appliance inherits base {
        include puppet
        include gitrepo
        include cobbler

        package { "rubygem-genomed":
            ensure      => installed,
        }

        service { "genomed":
            ensure      => running,
            hasrestart  => true,
            hasstatus   => true,
            require     => Package["rubygem-genomed"],
        }

        file { "/etc/genome":
             ensure    => directory,
        }
 
        # This allows all users to run puppetca --clean
        file { "/etc/sudoers":
            ensure      => present,
            owner       => "root",
            group       => "root",
            mode        => 0440,
            content     => template("genomerepo/sudoers.erb"),
            require     => User["$user"],
        }

        file { "/etc/genome/post-receive.template":
            ensure      => present,
            owner       => "root",
            group       => "root",
            mode        => 0744,
            source      => "puppet:///genomerepo/post-receive.template",
            require     => User[$user],
        }

        exec { "Setup post-recieve hooks for puppet modules":
            user        => $user,
            group       => $user,
            path        => "/usr/bin:/bin",
            command     => "find /pub/git/puppet -name hooks -type d -exec cp --preserve=mode /etc/genome/post-receive.template '{}'/post-receive \\;",
            require     => [User[$user], File["/etc/genome"], File["/pub/git/puppet"], File["/etc/genome/post-receive.template"]],
        } 

        # Technically the erb files require the cgi
        # Consider fixing that.
        genome::cgi::file { "puppetca.cgi": }
       
        # These files are used for cloning
        define genome::cgi::file {
            file { "/var/www/cgi-bin/${name}":
                owner       => "root",
                group       => "root",
                ensure      => present,
                mode        => 0755,
                require     => Service["httpd"],
                source      => "puppet:///genomerepo/${name}",
            }
        }
    }
}
