# Apparently there's an undocumented feature of Puppet related to module
# loading.  When the puppet module was changed from 'genome' to 'genomerepo'
# the following line was needed.
import "genomerepo"
include genomerepo::appliance
