Name:           genome-repo
Version:        1.0.2
Release:        1%{?dist}
Summary:        Genome repository 

Group:          Applications/System
License:        GPL 
URL:            http://genome-repo.usersys.redhat.com 
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  git
Requires:       puppet
Requires:       puppet-server
Requires:       cobbler <= 1.1.0
Requires:       yum-utils
Requires:       syslinux
Requires:       gitweb
Requires:       httpd
Requires:       git-daemon
#Technically we need git-all on Fedora 9
Requires:       git 
Requires:       rubygem-genomed
Requires:       rubygem-genome-sync
Requires:       genome-site
Requires:       genome-docs

%description
Barebones Genome repository.  See documentation for quick setup.


%prep
%setup -c 0

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/etc/genome
mkdir -p $RPM_BUILD_ROOT/etc/cobbler
mkdir -p $RPM_BUILD_ROOT/etc/puppet/modules
mkdir -p $RPM_BUILD_ROOT/etc/init.d
mkdir -p $RPM_BUILD_ROOT/pub/git/puppet

cp -r genome_module $RPM_BUILD_ROOT/etc/puppet/modules/genomerepo

cp repo.yaml $RPM_BUILD_ROOT/etc/genome
cp genome_module/files/machine_types.rb $RPM_BUILD_ROOT/etc/genome
cp bootstrap.pp $RPM_BUILD_ROOT/etc/genome
cp Genome.ks $RPM_BUILD_ROOT/etc/cobbler
cp Cloud.ks $RPM_BUILD_ROOT/etc/cobbler
cp Guest.ks $RPM_BUILD_ROOT/etc/cobbler
cp genome-repo-bootstrap $RPM_BUILD_ROOT/etc/init.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/etc/puppet/modules/genomerepo
/etc/genome/bootstrap.pp
/etc/cobbler/Genome.ks
/etc/cobbler/Cloud.ks
/etc/cobbler/Guest.ks
%config(noreplace) /etc/genome/repo.yaml
%config(noreplace) /etc/genome/machine_types.rb 

%defattr(755,root,root)
/etc/init.d/genome-repo-bootstrap

%doc

%changelog
