Name:           genome-release
Version:        1.0.1
Release:        2%{?dist}
Summary:        Genome repository configuration
Group:          System Environment/Base
License:        GPLv2
URL:            http://genome.et.redhat.com/
Source0:        RPM-GPG-KEY-genome
Source1:        genome.repo
Source2:        genome-devel.repo
Source3:        genome-testing.repo
Source4:        munge-repofiles.sh
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

%description
This package contains the Genome repository GPG key as well as configuration
for the Yum package manager.

%prep
%setup -c -T

%build
%{SOURCE4} %{SOURCE1} %{dist}
%{SOURCE4} %{SOURCE2} %{dist}
%{SOURCE4} %{SOURCE3} %{dist}

%install
rm -rf $RPM_BUILD_ROOT

# GPG
install -Dpm 644 %{SOURCE0} \
    $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-genome

# yum config
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 genome.repo $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
# We're not really making use of the devel and test Repos currently
#install -pm 644 genome.repo genome-devel.repo genome-testing.repo $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%post 
yum clean all

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_sysconfdir}/pki/rpm-gpg/*
%config %{_sysconfdir}/yum.repos.d/*

%changelog
* Mon Jul 28 2008 Brenton Leanhardt <bleanhar@redhat.com>
- Clear the yum cache in %post
* Wed Jul 23 2008 Brenton Leanhardt <bleanhar@redhat.com>
- Moved yum repositories to fedora people
* Tue Jul 15 2008 Brenton Leanhardt <bleanhar@redhat.com>
- initial pkg
