#!/bin/sh
# This is simply to keep us from having to maintain separate release RPMs for
# each distro.

case $2 in
.f*)
	sed -e "s/%%DIST%%/Fedora/" $1 > `basename $1`
	;;
*)
	sed -e "s/%%DIST%%/RHEL/" $1 > `basename $1`
	;;
esac
