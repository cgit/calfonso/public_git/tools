%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: Genome Func Applications
Name: genome-func
Source: genome-func-bin.tar.gz
Version: 1.0.0
Release: 3%{?dist}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Group: Applications/System
URL: https://fedorahosting.org/genome
License: GPLv2
BuildRequires: python-devel
Requires: func >= 0.21-2

%description
Genome Func Applications to support the cloudmasterd functions

%prep

%setup -c

%build

%install
# Cleaning up the build root
rm -rf $RPM_BUILD_ROOT

# Create the directory structure required to lay down our files
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/%{python_sitelib}/func/overlord

cp -R func-find-resources $RPM_BUILD_ROOT/usr/bin
cp -R find_resources.py $RPM_BUILD_ROOT/%{python_sitelib}/func/overlord

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/bin/func-find-resources
%{python_sitelib}/func/overlord/find_resources.py*

%doc
